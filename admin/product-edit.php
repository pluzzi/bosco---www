<?php
    include('isLogin.php');

    $page_id = 14;

    include('config/database-config.php');
    $sql = "select * from productos where id=".$_GET['id'];
    $results = $conn->query($sql);
    $product = mysqli_fetch_assoc($results);

?>
<!DOCTYPE html>
<html>
    <head>
        <?php include('head.php'); ?>
    </head>

    <body>
        <div id="wrapper">
            <?php include('side-menu.php'); ?>

            <div id="page-wrapper" class="gray-bg dashbard-1">
                <?php include('top-menu.php'); ?>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper wrapper-content">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Editar Productos de tienda</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="ibox-content">
                                    <div class="form-horizontal" >
                                        <div class="form-group"><label class="col-sm-2 control-label">Titulo</label>
                                            <div class="col-sm-10"><input type="text" class="form-control" id="titulo" value="<?php echo $product['titulo']; ?>"></div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Descripción</label>
                                            <div class="col-sm-10">
                                                <div class="summernote">

                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Mostrar stock</label>
                                            <div class="col-sm-10">
                                                <div class="i-checks"><input type="checkbox" id="mostrar_stock" <?php echo $product['mostrar_stock']==1 ? "checked" : ''; ?> ></div>
                                            </div>
                                        </div>

                                        <div class="form-group"><label class="col-sm-2 control-label">Stock</label>
                                            <div class="col-sm-10"><input type="text" class="form-control" id="stock" value="<?php echo $product['stock']; ?>"></div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Marca</label>
                                            <div class="col-sm-10">
                                                <select class="form-control" id="marca">
                                                    <?php
                                                        include('config/database-config.php');

                                                        $sql = "select id, descripcion from marcas";

                                                        $result = mysqli_query($conn, $sql);

                                                        while ($row = mysqli_fetch_assoc($result)) {
                                                            $selected = $product['brand_id'] == $row['id'] ? 'selected' : '';
                                                            echo '<option '.$selected.' data-id="'.$row['id'].'">'.$row['descripcion'].'</option>';
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Más vendido</label>
                                            <div class="col-sm-10">
                                                <div class="i-checks"><input type="checkbox" id="mas_vendido" <?php echo $product['mas_vendido']==1 ? "checked" : ''; ?> ></div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Envío gratis</label>
                                            <div class="col-sm-10">
                                                <div class="i-checks"><input type="checkbox" id="envio_gratis" <?php echo $product['envio_gratis']==1 ? "checked" : ''; ?>></div>
                                            </div>
                                        </div>

                                        <div class="form-group"><label class="col-sm-2 control-label">Precio</label>
                                            <div class="col-sm-10"><input type="text" class="form-control" id="precio" value="<?php echo $product['precio']; ?>"></div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php include('footer.php'); ?>

                    </div>
                </div>


            </div>
            
        </div>

        <?php include('scripts.php'); ?>

        <script>
            function update_data(value, column){
                if(value==null){
                    toastr.warning('Debe ingresar el campo '+column,'Validación');
                    return;
                }
                
                var $_GET = <?php echo json_encode($_GET); ?>;
                
                $.ajax({
                    url: "product-edit-save.php",
                    method: "POST",
                    data: {valor: value, columna: column, id: $_GET['id']},
                    success: function(results){ console.log(results)}
                });
            }

            $(document).on("blur", "#titulo", function(){
                var value = $(this).val();
                console.log(value);
                update_data(value, "titulo");
            });

            $('#mostrar_stock').change(function(){
                var value = $(this).is(":checked");
                console.log(value);
                update_data(value, "mostrar_stock");
            });

            $(document).on("blur", "#stock", function(){
                var value = $(this).val();
                console.log(value);
                update_data(value, "stock");
            });

            $('#marca').on('change', function(){
                var marca = $('#marca').find(':selected').data('id');
                console.log(marca);
                update_data(marca, "brand_id");
            });

            $('#mas_vendido').change(function(){
                var value = $(this).is(":checked");
                console.log(value);
                update_data(value, "mas_vendido");
            });

            $('#envio_gratis').change(function(){
                var value = $(this).is(":checked");
                console.log(value);
                update_data(value, "envio_gratis");
            });

            $(document).on("blur", "#precio", function(){
                var value = $(this).val();
                console.log(value);
                update_data(value, "precio");
            });

            $(document).ready(function() {
                $('.summernote').summernote({
                    height: 200
                });
                var value = <?php echo json_encode($product['descripcion']); ?>;

                $(".summernote").summernote("code", value);
            });

            $(".summernote").on("summernote.change", function (e) {   // callback as jquery custom event
                var descripcion = $('.summernote').summernote('code');
                console.log(descripcion);
                update_data(descripcion, "descripcion");
            });

        </script>

    </body>
</html>