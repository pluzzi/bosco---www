<?php
    include('isLogin.php');

    $page_id = 2;
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include('head.php'); ?>
    </head>

    <body>
        <div id="wrapper">
            <?php include('side-menu.php'); ?>

            <div id="page-wrapper" class="gray-bg dashbard-1">
                <?php include('top-menu.php'); ?>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper wrapper-content">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Nuevo Carrusel</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="ibox-content">
                                    <div class="form-horizontal" >

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Texto</label>
                                            <div class="col-sm-10">
                                                <div class="summernote">
                                                    
                                                </div>        
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Marca</label>
                                            <div class="col-sm-10">
                                                <select class="form-control" id="marca">
                                                    <?php
                                                        include('config/database-config.php');

                                                        $sql = "select id, descripcion from marcas";

                                                        $result = mysqli_query($conn, $sql);

                                                        while ($row = mysqli_fetch_assoc($result)) {
                                                            echo '<option data-id="'.$row['id'].'">'.$row['descripcion'].'</option>';
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Imagen</label>
                                            <div class="col-sm-10">
                                                <h4>Preview</h4>
                                                <img class="img-preview img-preview-sm" id="preview"/>

                                                <label title="Upload image file" for="inputImage" class="btn btn-primary">
                                                    <input onchange="readURL(this);" accept="image/*" type="file"  id="user-img">
                                                </label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-4">
                                                <button class="btn btn-primary" id="save" >Guardar</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php include('footer.php'); ?>

                    </div>
                </div>

                <?php include('chat.php'); ?>

            </div>
            
        </div>

        <?php include('scripts.php'); ?>

        <script>
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#preview').attr('src', e.target.result);
                    };

                    reader.readAsDataURL(input.files[0]);
                }
            }

            $(document).ready(function() {
                $('.summernote').summernote({
                    height: 200
                });
            });

            $(document).on("click", "#save", function(){
                var texto = $('.summernote').summernote('code');
                var marca = $('#marca').find(':selected').data('id');

                console.log(texto);

                if(texto==null || texto.trim()==""){
                    toastr.warning('Debe ingresar un texto.','Validación');
                    return;
                }
                
                $.ajax({
                    url: "carrusel-add-save.php",
                    method: "POST",
                    data: {texto: texto, marca: marca},
                    success: function(id){
                        var file_data = $("#user-img").prop('files')[0];   
                        var form_data = new FormData();                  
                        form_data.append('file', file_data);

                        $.ajax({
                            url: 'carrusel-add-save-img.php?id='+id,
                            dataType: 'text',
                            cache: false,
                            contentType: false,
                            processData: false,
                            data: form_data,                         
                            type: 'POST',
                            success: function(result){
                                console.log(result);
                                window.location.href = "carrusel.php";
                            }
                        });
                    }
                });
            });
        </script>

    </body>
</html>
