<?php
    include('isLogin.php');

    $page_id = 13;

    include('config/database-config.php');
    $sql = "select id, codigo, descripcion, logo, dolar from marcas where id=".$_GET['id'];
    $resultado = $conn->query($sql);
    $marca = mysqli_fetch_assoc($resultado);

?>
<!DOCTYPE html>
<html>
    <head>
        <?php include('head.php'); ?>
    </head>

    <body>
        <div id="wrapper">
            <?php include('side-menu.php'); ?>

            <div id="page-wrapper" class="gray-bg dashbard-1">
                <?php include('top-menu.php'); ?>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper wrapper-content">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Editar Marca</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="ibox-content">
                                    <div class="form-horizontal" >
                                        <div class="form-group"><label class="col-sm-2 control-label">Código</label>
                                            <div class="col-sm-10"><input type="text" class="form-control" id="codigo" value="<?php echo $marca['codigo']; ?>"></div>
                                        </div>

                                        <div class="form-group"><label class="col-sm-2 control-label">Descripción</label>
                                            <div class="col-sm-10"><input type="text" class="form-control" id="descripcion" value="<?php echo $marca['descripcion']; ?>"></div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Logo</label>
                                            <div class="col-sm-10">
                                                <h4>Preview</h4>
                                                <?php echo '<img class="img-preview img-preview-sm" id="preview" src="data:image/jpeg;base64,'.base64_encode( $marca['logo'] ).'" />'; ?>

                                                <label title="Upload image file" for="inputImage" class="btn btn-primary">
                                                    <input onchange="readURL(this);" accept="image/*" type="file"  id="brand-img">
                                                </label>
                                            </div>
                                        </div>

                                        <div class="form-group"><label class="col-sm-2 control-label">Dolar</label>
                                            <div class="col-sm-10"><input type="text" class="form-control" id="dolar" value="<?php echo $marca['dolar']; ?>"></div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php include('footer.php'); ?>

                    </div>
                </div>

                <?php include('chat.php'); ?>

            </div>
            
        </div>

        <?php include('scripts.php'); ?>

        <script>
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#preview').attr('src', e.target.result);
                    };

                    reader.readAsDataURL(input.files[0]);

                    saveImg();
                }
            }

            function saveImg() {
                var file_data = $("#brand-img").prop('files')[0];
                var form_data = new FormData();
                form_data.append('file', file_data);

                var $_GET = <?php echo json_encode($_GET); ?>;

                $.ajax({
                    url: 'brand-add-save-img.php?id='+$_GET['id'],
                    dataType: 'text',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    type: 'POST',
                    success: function(result){ }
                });
            }


            function update_data(value, column){
                if(value==null || value.trim()==""){
                    toastr.warning('Debe ingresar el campo '+column,'Validación');
                    return;
                }

                var $_GET = <?php echo json_encode($_GET); ?>;
                
                $.ajax({
                    url: "brand-edit-save.php",
                    method: "POST",
                    data: {valor: value, columna: column, id: $_GET['id']},
                    success: function(results){}
                });
            }

            $(document).on("blur", "#codigo", function(){
                var codigo = $(this).val();
                update_data(codigo, "codigo");
            });

            $(document).on("blur", "#descripcion", function(){
                var descripcion = $(this).val();
                update_data(descripcion, "descripcion");
            });

            $(document).on("blur", "#dolar", function(){
                var dolar = $(this).val();
                update_data(dolar, "dolar");
            });

        </script>

    </body>
</html>