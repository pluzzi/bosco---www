<?php
    session_start();

    include('config/database-config.php');
        
    $sql = "select id, nombre, roll from comentarios_clientes";

    $result = mysqli_query($conn, $sql);

    if($result){
        while ($row = mysqli_fetch_assoc($result)) {
            echo '<tr>
                <td>'.$row['id'] .'</td>
                <td>'.$row['nombre'] .'</td>
                <td>'.$row['roll'] .'</td>
                <td>
                    <button id="edit" class="btn btn-primary btn-sm" data-id="'.$row['id'].'">
                        <i class="fa fa-edit"></i>
                    </button>
                    <button id="delete" class="btn btn-primary btn-sm" data-id="'.$row['id'].'">
                        <i class="fa fa-minus-circle"></i>
                    </button>
                </td>
            </tr>';
        }
    }

?>
