<?php
    include('isLogin.php');

    $page_id = 2;

    include('config/database-config.php');
    $sql = "select id, img, texto,brand_id from carrusel where id=".$_GET['id'];
    $resultado = $conn->query($sql);
    $carrusel = mysqli_fetch_assoc($resultado);

?>
<!DOCTYPE html>
<html>
    <head>
        <?php include('head.php'); ?>
    </head>

    <body>
        <div id="wrapper">
            <?php include('side-menu.php'); ?>

            <div id="page-wrapper" class="gray-bg dashbard-1">
                <?php include('top-menu.php'); ?>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper wrapper-content">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Editar Carrusel</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="ibox-content">
                                    <div class="form-horizontal" >
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Texto</label>
                                            <div class="col-sm-10">
                                                <div class="summernote">
                                                    
                                                </div>        
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Marca</label>
                                            <div class="col-sm-10">
                                                <select class="form-control" id="marca">
                                                    <?php
                                                        include('config/database-config.php');

                                                        $sql = "select id, descripcion from marcas";

                                                        $result = mysqli_query($conn, $sql);

                                                        while ($row = mysqli_fetch_assoc($result)) {
                                                            $selected = $carrusel['brand_id'] == $row['id'] ? 'selected' : '';
                                                            echo '<option '.$selected.' data-id="'.$row['id'].'">'.$row['descripcion'].'</option>';
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        
                                        
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Imagen</label>
                                            <div class="col-sm-10">
                                                <h4>Preview</h4>
                                                <?php echo '<img class="img-preview img-preview-sm" id="preview" src="data:image/jpeg;base64,'.base64_encode( $carrusel['img'] ).'" />'; ?>

                                                <label title="Upload image file" for="inputImage" class="btn btn-primary">
                                                    <input onchange="readURL(this);" accept="image/*" type="file"  id="carrusel-img">
                                                </label>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php include('footer.php'); ?>

                    </div>
                </div>

                <?php include('chat.php'); ?>

            </div>
            
        </div>

        <?php include('scripts.php'); ?>

        <script>
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#preview').attr('src', e.target.result);
                    };

                    reader.readAsDataURL(input.files[0]);

                    saveImg();
                }
            }

            function saveImg() {
                var file_data = $("#carrusel-img").prop('files')[0];   
                var form_data = new FormData();                  
                form_data.append('file', file_data);

                var $_GET = <?php echo json_encode($_GET); ?>;

                $.ajax({
                    url: 'carrusel-add-save-img.php?id='+$_GET['id'],
                    dataType: 'text',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,                         
                    type: 'POST',
                    success: function(result){ }
                });
            }

            $(document).ready(function() {
                $('.summernote').summernote({
                    height: 200
                });
                var value = <?php echo json_encode($carrusel['texto']); ?>;
                
                $(".summernote").summernote("code", value);
            });

            function update_data(value, column){
                if(value==null){
                    toastr.warning('Debe ingresar el campo '+column,'Validación');
                    return;
                }
                var $_GET = <?php echo json_encode($_GET); ?>;

                $.ajax({
                    url: "carrusel-edit-save.php",
                    method: "POST",
                    data: {valor: value, columna: column, id: $_GET['id']},
                    success: function(results){ console.log(results);}
                });
            }

            $(document).on("blur", ".summernote", function(){
                var texto = $('.summernote').summernote('code');
                console.log(texto);
                update_data(texto, "texto");
            })



            $('#marca').on('change', function(){
                var marca = $('#marca').find(':selected').data('id');
                console.log(marca);
                update_data(marca, "brand_id");
            });
            

        </script>

    </body>
</html>