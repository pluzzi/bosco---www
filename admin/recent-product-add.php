<?php
    include('isLogin.php');

    $page_id = 8;
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include('head.php'); ?>
    </head>

    <body>
        <div id="wrapper">
            <?php include('side-menu.php'); ?>

            <div id="page-wrapper" class="gray-bg dashbard-1">
                <?php include('top-menu.php'); ?>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper wrapper-content">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Agregar Producto Reciente</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="ibox-content">
                                    <div class="form-horizontal" >
                                        <div class="form-group"><label class="col-sm-2 control-label">Titulo</label>
                                            <div class="col-sm-10"><input type="text" class="form-control" id="titulo"></div>
                                        </div>

                                        <div class="form-group"><label class="col-sm-2 control-label">Descripcion</label>
                                            <div class="col-sm-10"><input type="text" class="form-control" id="descripcion"></div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-4">
                                                <button class="btn btn-primary" id="save" >Guardar</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php include('footer.php'); ?>

                    </div>
                </div>

                <?php include('chat.php'); ?>

            </div>
            
        </div>

        <?php include('scripts.php'); ?>

        <script>
            $(document).ready(function() {

            });

            $(document).on("click", "#save", function(){
                var titulo = $("#titulo").val();
                var descripcion = $("#descripcion").val();

                var msg = "";

                if(titulo==null || titulo.trim()==""){
                    msg += 'Debe ingresar un título.<br>';
                }
                if(descripcion==null || descripcion.trim()==""){
                    msg += 'Debe ingresar una descripcion.<br>';
                }
                if(msg!=""){
                    toastr.warning(msg,'Validación');
                    return;
                }

                $.ajax({
                    url: "recent-product-add-save.php",
                    method: "POST",
                    data: {titulo: titulo, descripcion: descripcion},
                    success: function(id){
                        window.location.href = "recent-product.php";
                    }
                });
            });
        </script>

    </body>
</html>
