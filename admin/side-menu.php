<?php
    include('config/database-config.php');
    $sql = "select nombre, img from usuarios where id =".$_SESSION['id'];
    $result = $conn->query($sql);
    $usuario = mysqli_fetch_assoc($result)
?>
<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element"> 
                    <span>
                        <?php echo '<img alt="image" class="img-circle img-profile-size" src="data:image/jpeg;base64,'.base64_encode( $usuario['img'] ).'" />'; ?>                    
                    </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear">
                            <span class="block m-t-xs"><strong class="font-bold"><?php echo substr($usuario['nombre'],0,strpos($usuario['nombre'], '@')); ?></strong></span>
                            <span class="text-muted text-xs block">Administrador<?php //echo $usuario['descripcion']; ?> <b class="caret"></b></span>
                        </span>
                    </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a href="profile.php">Profile</a></li>
                        <li><a href="mailbox.php">Mailbox</a></li>
                        <li class="divider"></li>
                        <li><a href="logout.php">Logout</a></li>
                    </ul>
                </div>
                <div class="logo-element">
                    MB
                </div>
            </li>

            <li class="<?php if($page_id==0){ echo "active"; } ?>">
                <a href="index.php"><i class="fa fa-home"></i> <span class="nav-label">Inicio</span></a>
            </li>

            <li class="<?php if($page_id==4){ echo "active"; } ?>">
                <a href="users.php"><i class="fa fa-users"></i> <span class="nav-label">Usuarios</span></a>
            </li>

            <li class="<?php if($page_id==2){ echo "active"; } ?>">
                <a href="carrusel.php"><i class="fa fa-file-image-o"></i> <span class="nav-label">Carrusel</span></a>
            </li>

            <li class="<?php if($page_id==3){ echo "active"; } ?>">
                <a href="us.php"><i class="fa fa-chain"></i> <span class="nav-label">A que nos dedicamos</span></a>
            </li>

            <li class="<?php if($page_id==5){ echo "active"; } ?>">
                <a href="products-services.php"><i class="fa fa-coffee"></i> <span class="nav-label">Productos Y Servicio</span></a>
            </li>

            <li class="<?php if($page_id==8){ echo "active"; } ?>">
                <a href="recent-product.php"><i class="fa fa-flash"></i> <span class="nav-label">Productos Recientes</span></a>
            </li>

            <li class="<?php if($page_id==9){ echo "active"; } ?>">
                <a href="information-edit.php"><i class="fa fa-child"></i> <span class="nav-label">Información</span></a>
            </li>

            <!--
            <li class="<?php if($page_id==10){ echo "active"; } ?>">
                <a href="team.php"><i class="fa fa-group"></i> <span class="nav-label">Equipo</span></a>
            </li>-->

            <!--
            <li class="<?php if($page_id==11){ echo "active"; } ?>">
                <a href="customer-comment.php"><i class="fa fa-comment"></i> <span class="nav-label">Comentarios de Clientes</span></a>
            </li>-->

            <!--
            <li class="<?php if($page_id==12){ echo "active"; } ?>">
                <a href="customer-image.php"><i class="fa fa-image"></i> <span class="nav-label">Logos de Clientes</span></a>
            </li>-->

            <li class="<?php if($page_id==13){ echo "active"; } ?>">
                <a href="brand.php"><i class="fa fa-user"></i> <span class="nav-label">Marcas</span></a>
            </li>

            <li class="<?php if($page_id==14){ echo "active"; } ?>">
                <a href="product.php"><i class="fa fa-shopping-cart"></i> <span class="nav-label">Productos</span></a>
            </li>

            <li class="<?php if($page_id==15){ echo "active"; } ?>">
                <a href="discount.php"><i class="fa fa-dollar"></i> <span class="nav-label">Descuentos</span></a>
            </li>

        </ul>

    </div>
</nav>