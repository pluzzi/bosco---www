<?php
    session_start();

    include('config/database-config.php');
        
    $sql = "select * from productos";

    $result = mysqli_query($conn, $sql) or die (mysqli_error());

    while ($row = mysqli_fetch_assoc($result)) {
        echo '<tr>
                <td>'.$row['id'] .'</td>
                <td>'.$row['titulo'] .'</td>
                <td>
                    <button id="image" class="btn btn-primary btn-sm" data-id="'.$row['id'].'">
                        <i class="fa fa-file-image-o"></i>
                    </button>
                    <button id="edit" class="btn btn-primary btn-sm" data-id="'.$row['id'].'">
                        <i class="fa fa-edit"></i>
                    </button>
                    <button id="delete" class="btn btn-primary btn-sm" data-id="'.$row['id'].'">
                        <i class="fa fa-minus-circle"></i>
                    </button>
                </td>
            </tr>';
    }
?>
