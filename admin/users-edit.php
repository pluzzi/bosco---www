<?php
    include('isLogin.php');

    $page_id = 4;

    include('config/database-config.php');
    $sql = "select id, nombre, img from usuarios where id=".$_GET['id'];
    $resultado = $conn->query($sql);
    $usuario_actual = mysqli_fetch_assoc($resultado);

?>
<!DOCTYPE html>
<html>
    <head>
        <?php include('head.php'); ?>
    </head>

    <body>
        <div id="wrapper">
            <?php include('side-menu.php'); ?>

            <div id="page-wrapper" class="gray-bg dashbard-1">
                <?php include('top-menu.php'); ?>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper wrapper-content">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Editar Usuario</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="ibox-content">
                                    <div class="form-horizontal" >
                                        <div class="form-group"><label class="col-sm-2 control-label">Email</label>
                                            <div class="col-sm-10"><input type="email" class="form-control" id="nombre" value="<?php echo $usuario_actual['nombre']; ?>"></div>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Imagen</label>
                                            <div class="col-sm-10">
                                                <h4>Preview</h4>
                                                <?php echo '<img class="img-preview img-preview-sm" id="preview" src="data:image/jpeg;base64,'.base64_encode( $usuario_actual['img'] ).'" />'; ?>

                                                <label title="Upload image file" for="inputImage" class="btn btn-primary">
                                                    <input onchange="readURL(this);" accept="image/*" type="file"  id="user-img">
                                                </label>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php include('footer.php'); ?>

                    </div>
                </div>

                <?php include('chat.php'); ?>

            </div>
            
        </div>

        <?php include('scripts.php'); ?>

        <script>
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#preview').attr('src', e.target.result);
                    };

                    reader.readAsDataURL(input.files[0]);

                    saveImg();
                }
            }

            function saveImg() {
                var file_data = $("#user-img").prop('files')[0];   
                var form_data = new FormData();                  
                form_data.append('file', file_data);

                var $_GET = <?php echo json_encode($_GET); ?>;

                $.ajax({
                    url: 'users-add-save-img.php?id='+$_GET['id'],
                    dataType: 'text',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,                         
                    type: 'POST',
                    success: function(result){ }
                });
            }

            function update_data(value, column){
                if(value==null || value.trim()==""){
                    toastr.warning('Debe ingresar el campo '+column,'Validación');
                    return;
                }

                var $_GET = <?php echo json_encode($_GET); ?>;
                
                $.ajax({
                    url: "users-edit-save.php",
                    method: "POST",
                    data: {valor: value, columna: column, id: $_GET['id']},
                    success: function(results){}
                });
            }

            $(document).on("blur", "#nombre", function(){
                var nombre = $(this).val();
                update_data(nombre, "nombre");
            })

        </script>

    </body>
</html>