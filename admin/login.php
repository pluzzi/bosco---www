<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Bosco | Login</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>
                <?php
                    if(isset($_GET['error'])){
                        if($_GET['error']=="1"){
                            echo '<div class="alert alert-danger" role="alert">
                                    El usuario o la contraseña no son correctos.
                                </div>';
                        }
                    }
                ?>
                <h1 class="logo-name">B</h1>

            </div>
            <h3>Bienvenido a Bosco</h3>
            <p>Ingrese su usuario y contraseña para ingresar al panel de administrador de Bosco</p>
            
            <form class="m-t" role="form" action="autenticar.php" method="post">
                <div class="form-group">
                    <input type="email" class="form-control" placeholder="Usuario" required="" name="user">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" placeholder="Contraseña" required="" name="password">
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">Ingresar</button>

                <a href="#"><small>Olvido su contraseña?</small></a>
                
            </form>
            <p class="m-t"> <small>Bosco diseñado por Patricio Luzzi &copy; 2019</small> </p>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
