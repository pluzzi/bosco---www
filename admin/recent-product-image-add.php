<?php
    include('isLogin.php');

    $page_id = 8;

    $id = $_GET['id'];

?>
<!DOCTYPE html>
<html>
    <head>
        <?php include('head.php'); ?>
    </head>

    <body>
        <div id="wrapper">
            <?php include('side-menu.php'); ?>

            <div id="page-wrapper" class="gray-bg dashbard-1">
                <?php include('top-menu.php'); ?>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper wrapper-content">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Nueva Imágen de Producto Reciente</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="ibox-content">
                                    <div class="form-horizontal" >
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Imágen</label>
                                            <div class="col-sm-10">
                                                <h4>Preview</h4>
                                                <img class="img-preview img-preview-sm" id="preview"/>

                                                <label title="Upload image file" for="inputImage" class="btn btn-primary">
                                                    <input onchange="readURL(this);" accept="image/*" type="file"  id="imagen">
                                                </label>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <div class="col-sm-2">
                                                <button class="btn btn-primary" id="volver" >Volver</button>
                                            </div>
                                            <div class="col-sm-2">
                                                <button class="btn btn-primary" id="save" >Guardar</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php include('footer.php'); ?>

                    </div>
                </div>

                <?php include('chat.php'); ?>

            </div>
            
        </div>

        <?php include('scripts.php'); ?>

        <script>
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#preview').attr('src', e.target.result);
                    };

                    reader.readAsDataURL(input.files[0]);
                }
            }

            $(document).ready(function() {

            });

            $(document).on("click", "#save", function(){
                var file_data = $("#imagen").prop('files')[0];
                var form_data = new FormData();
                form_data.append('file', file_data);
                var id = <?php echo $id; ?>;

                $.ajax({
                    url: 'recent-product-image-add-save-img.php?id='+id,
                    dataType: 'text',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    type: 'POST',
                    success: function(result){
                        console.log(result);
                        var id = <?php echo $id; ?>;
                        window.location.href = "recent-product-images.php?id="+id;
                    }
                });

            });

            $(document).on("click", "#volver", function(){
                var id = <?php echo $id; ?>;
                window.location.href = "recent-product-images.php?id="+id;
            });
        </script>

    </body>
</html>
