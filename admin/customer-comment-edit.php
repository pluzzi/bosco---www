<?php
    include('isLogin.php');

    $page_id = 11;

    include('config/database-config.php');
    $sql = "select id, comentario, nombre, roll from comentarios_clientes where id=".$_GET['id'];
    $resultado = $conn->query($sql);
    $comment = mysqli_fetch_assoc($resultado);

?>
<!DOCTYPE html>
<html>
    <head>
        <?php include('head.php'); ?>
    </head>

    <body>
        <div id="wrapper">
            <?php include('side-menu.php'); ?>

            <div id="page-wrapper" class="gray-bg dashbard-1">
                <?php include('top-menu.php'); ?>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper wrapper-content">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Editar Comentario de Cliente</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="ibox-content">
                                    <div class="form-horizontal" >
                                        <div class="form-group"><label class="col-sm-2 control-label">Comentario</label>
                                            <div class="col-sm-10"><input type="text" class="form-control" id="comentario" value="<?php echo $comment['comentario'] ?>"></div>
                                        </div>

                                        <div class="form-group"><label class="col-sm-2 control-label">Nombre</label>
                                            <div class="col-sm-10"><input type="text" class="form-control" id="nombre" value="<?php echo $comment['nombre'] ?>"></div>
                                        </div>

                                        <div class="form-group"><label class="col-sm-2 control-label">Roll</label>
                                            <div class="col-sm-10"><input type="text" class="form-control" id="roll" value="<?php echo $comment['roll'] ?>"></div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php include('footer.php'); ?>

                    </div>
                </div>

                <?php include('chat.php'); ?>

            </div>
            
        </div>

        <?php include('scripts.php'); ?>

        <script>
            function update_data(value, column){
                if(value==null || value.trim()==""){
                    toastr.warning('Debe ingresar el campo '+column,'Validación');
                    return;
                }

                var $_GET = <?php echo json_encode($_GET); ?>;
                
                $.ajax({
                    url: "customer-comment-edit-save.php",
                    method: "POST",
                    data: {valor: value, columna: column, id: $_GET['id']},
                    success: function(results){
                        console.log(results);
                    }
                });
            }

            $(document).on("blur", "#comentario", function(){
                var value = $(this).val();
                update_data(value, "comentario");
            })
            $(document).on("blur", "#nombre", function(){
                var value = $(this).val();
                update_data(value, "nombre");
            })
            $(document).on("blur", "#roll", function(){
                var value = $(this).val();
                update_data(value, "roll");
            })

        </script>

    </body>
</html>