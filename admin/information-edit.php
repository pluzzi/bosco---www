<?php
    include('isLogin.php');

    $page_id = 9;

    include('config/database-config.php');
    $sql = "select * from informacion limit 1";
    $results = $conn->query($sql);
    $informacion = mysqli_fetch_assoc($results);

?>
<!DOCTYPE html>
<html>
    <head>
        <?php include('head.php'); ?>
    </head>

    <body>
        <div id="wrapper">
            <?php include('side-menu.php'); ?>

            <div id="page-wrapper" class="gray-bg dashbard-1">
                <?php include('top-menu.php'); ?>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper wrapper-content">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Editar Información Acerca de nosotros</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="ibox-content">
                                    <div class="form-horizontal" >
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Descripción</label>
                                            <div class="col-sm-10">
                                                <div class="summernote" >
                                                    
                                                </div>        
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php include('footer.php'); ?>

                    </div>
                </div>


            </div>
            
        </div>

        <?php include('scripts.php'); ?>

        <script>
            function update_data(value, column){
                if(value==null || value.trim()==""){
                    toastr.warning('Debe ingresar el campo '+column,'Validación');
                    return;
                }

                $.ajax({
                    url: "information-edit-save.php",
                    method: "POST",
                    data: {valor: value, columna: column},
                    success: function(results){ console.log(results); }
                });
            }

            $(document).ready(function() {
                $('.summernote').summernote({
                    height: 200
                });
                var value = <?php echo json_encode($informacion['descripcion']); ?>;
                $(".summernote").summernote("code", value);
            });

            $(".summernote").on("summernote.change", function (e) {   // callback as jquery custom event
                var value = $('.summernote').summernote('code');
                update_data(value, "descripcion");
            });

        </script>

    </body>
</html>