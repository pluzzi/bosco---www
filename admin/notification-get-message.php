<?php
    include("config/database-config.php");
    $sql = "select id, mensaje, usuario, TIMESTAMPDIFF(MINUTE , fecha, NOW()) as dif, orden from notificaciones where visto = 0";
    $results = mysqli_query($conn, $sql);

    while($row = mysqli_fetch_array($results)){

        echo '<li>
                <a href="notification-detail.php?id='.$row['id'].'&orden='.$row['orden'].'">
                    <div>
                        <i class="fa fa-money"></i> '.$row['mensaje'].'
                        <span class="pull-right text-muted small">Hace '.$row['dif'].' minutos</span>
                    </div>
                </a>
            </li><li class="divider"></li>';
    }

    mysqli_free_result($results);
    mysqli_close($conn);

?>