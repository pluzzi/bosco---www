<?php
    include('isLogin.php');

    $page_id = 10;

    include('config/database-config.php');
    $sql = "select * from equipo where id=".$_GET['id'];
    $resultado = $conn->query($sql);
    $member = mysqli_fetch_assoc($resultado);

?>
<!DOCTYPE html>
<html>
    <head>
        <?php include('head.php'); ?>
    </head>

    <body>
        <div id="wrapper">
            <?php include('side-menu.php'); ?>

            <div id="page-wrapper" class="gray-bg dashbard-1">
                <?php include('top-menu.php'); ?>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper wrapper-content">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Editar Miembro del Equipo</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="ibox-content">
                                    <div class="form-horizontal" >
                                        <div class="form-group"><label class="col-sm-2 control-label">Nombre</label>
                                            <div class="col-sm-10"><input type="text" class="form-control" id="nombre" value="<?php echo $member['nombre']; ?>"></div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Foto</label>
                                            <div class="col-sm-10">
                                                <h4>Preview</h4>
                                                <?php echo '<img class="img-preview img-preview-sm" id="preview-img" src="data:image/jpeg;base64,'.base64_encode( $member['foto'] ).'" />'; ?>

                                                <label title="Upload image file" class="btn btn-primary">
                                                    <input onchange="readURL(this, 1)" accept="image/*" type="file"  id="member-img">
                                                </label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Fondo</label>
                                            <div class="col-sm-10">
                                                <h4>Preview</h4>
                                                <?php echo '<img class="img-preview img-preview-sm" id="preview-bg" src="data:image/jpeg;base64,'.base64_encode( $member['fondo'] ).'" />'; ?>

                                                <label title="Upload image file" class="btn btn-primary">
                                                    <input onchange="readURL(this, 2)" accept="image/*" type="file"  id="member-bg">
                                                </label>
                                            </div>
                                        </div>

                                        <div class="form-group"><label class="col-sm-2 control-label">Descricion</label>
                                            <div class="col-sm-10"><input type="text" class="form-control" id="descripcion" value="<?php echo $member['descripcion']; ?>"></div>
                                        </div>

                                        <div class="form-group"><label class="col-sm-2 control-label">Facebook</label>
                                            <div class="col-sm-10"><input type="text" class="form-control" id="facebook" value="<?php echo $member['facebook']; ?>"></div>
                                        </div>

                                        <div class="form-group"><label class="col-sm-2 control-label">Twitter</label>
                                            <div class="col-sm-10"><input type="text" class="form-control" id="twitter" value="<?php echo $member['twitter']; ?>"></div>
                                        </div>

                                        <div class="form-group"><label class="col-sm-2 control-label">Linkedin</label>
                                            <div class="col-sm-10"><input type="text" class="form-control" id="linkedin" value="<?php echo $member['linkedin']; ?>"></div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php include('footer.php'); ?>

                    </div>
                </div>

                <?php include('chat.php'); ?>

            </div>
            
        </div>

        <?php include('scripts.php'); ?>

        <script>
            function readURL(input, i) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        if(i==1){
                            $('#preview-img').attr('src', e.target.result);
                        }else{
                            $('#preview-bg').attr('src', e.target.result);
                        }
                    };

                    reader.readAsDataURL(input.files[0]);

                    saveImg(i);
                }
            }

            function saveImg(i) {
                var file_data = null;

                if(i==1){
                    file_data = $("#member-img").prop('files')[0];
                }else{
                    file_data = $("#member-bg").prop('files')[0];
                }

                var form_data = new FormData();                  
                form_data.append('file', file_data);

                var $_GET = <?php echo json_encode($_GET); ?>;

                $.ajax({
                    url: 'team-add-save-img.php?id='+$_GET['id']+'&code='+i,
                    dataType: 'text',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,                         
                    type: 'POST',
                    success: function(result){ }
                });
            }

            function update_data(value, column){
                if(value==null || value.trim()==""){
                    toastr.warning('Debe ingresar el campo '+column,'Validación');
                    return;
                }

                var $_GET = <?php echo json_encode($_GET); ?>;
                
                $.ajax({
                    url: "team-edit-save.php",
                    method: "POST",
                    data: {valor: value, columna: column, id: $_GET['id']},
                    success: function(results){}
                });
            }

            $(document).on("blur", "#nombre", function(){
                var nombre = $(this).val();
                update_data(nombre, "nombre");
            })
            $(document).on("blur", "#descripcion", function(){
                var descripcion = $(this).val();
                update_data(descripcion, "descripcion");
            })
            $(document).on("blur", "#facebook", function(){
                var facebook = $(this).val();
                update_data(facebook, "facebook");
            })
            $(document).on("blur", "#twitter", function(){
                var twitter = $(this).val();
                update_data(twitter, "twitter");
            })
            $(document).on("blur", "#linkedin", function(){
                var linkedin = $(this).val();
                update_data(linkedin, "linkedin");
            })

        </script>

    </body>
</html>