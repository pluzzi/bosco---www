<?php
    include('isLogin.php');

    $page_id = 15;

    include('config/database-config.php');
    $sql = "select * from descuentos_producto where id=".$_GET['id'];
    $results = $conn->query($sql);
    $descuento = mysqli_fetch_assoc($results);

?>
<!DOCTYPE html>
<html>
    <head>
        <?php include('head.php'); ?>
    </head>

    <body>
        <div id="wrapper">
            <?php include('side-menu.php'); ?>

            <div id="page-wrapper" class="gray-bg dashbard-1">
                <?php include('top-menu.php'); ?>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper wrapper-content">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Editar Descuento</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="ibox-content">
                                    <div class="form-horizontal" >
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Producto</label>
                                            <div class="col-sm-10">
                                                <select class="form-control" id="producto">
                                                    <?php
                                                    include('config/database-config.php');

                                                    $sql = "select id, titulo from productos";

                                                    $result = mysqli_query($conn, $sql);

                                                    while ($row = mysqli_fetch_assoc($result)) {
                                                        $selected = $descuento['producto'] == $row['id'] ? 'selected' : '';
                                                        echo '<option '.$selected.' data-id="'.$row['id'].'">'.$row['titulo'].'</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Usuario</label>
                                            <div class="col-sm-10">
                                                <select class="form-control" id="usuario">
                                                    <?php
                                                    include('config/database-config.php');

                                                    $sql = "select id, nombre, apellido from usuarios_ec";

                                                    $result = mysqli_query($conn, $sql);

                                                    while ($row = mysqli_fetch_assoc($result)) {
                                                        $selected = $descuento['usuario'] == $row['id'] ? 'selected' : '';
                                                        echo '<option '.$selected.' data-id="'.$row['id'].'">'.$row['nombre'].', '.$row['apellido'].'</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group"><label class="col-sm-2 control-label">Descuento</label>
                                            <div class="col-sm-10"><input type="text" class="form-control" id="descuento" value="<?php echo $descuento['descuento']; ?>"></div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php include('footer.php'); ?>

                    </div>
                </div>


            </div>
            
        </div>

        <?php include('scripts.php'); ?>

        <script>
            function update_data(value, column){
                if(value==null){
                    toastr.warning('Debe ingresar el campo '+column,'Validación');
                    return;
                }
                
                var $_GET = <?php echo json_encode($_GET); ?>;
                
                $.ajax({
                    url: "discount-edit-save.php",
                    method: "POST",
                    data: {valor: value, columna: column, id: $_GET['id']},
                    success: function(results){ console.log(results)}
                });
            }

            $('#producto').on('change', function(){
                var producto = $('#producto').find(':selected').data('id');
                console.log(producto);
                update_data(producto, "producto");
            });

            $('#usuario').on('change', function(){
                var usuario = $('#usuario').find(':selected').data('id');
                console.log(usuario);
                update_data(usuario, "usuario");
            });

            $(document).on("blur", "#descuento", function(){
                var descuento = $(this).val();
                console.log(descuento);
                update_data(descuento, "descuento");
            });


        </script>

    </body>
</html>