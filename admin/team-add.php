<?php
    include('isLogin.php');

    $page_id = 10;
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include('head.php'); ?>
    </head>

    <body>
        <div id="wrapper">
            <?php include('side-menu.php'); ?>

            <div id="page-wrapper" class="gray-bg dashbard-1">
                <?php include('top-menu.php'); ?>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper wrapper-content">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Nuevo Integrante del Equipo</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="ibox-content">
                                    <div class="form-horizontal" >
                                        <div class="form-group"><label class="col-sm-2 control-label">Nombre</label>
                                            <div class="col-sm-10"><input type="text" class="form-control" id="nombre"></div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Foto</label>
                                            <div class="col-sm-10">
                                                <h4>Preview</h4>
                                                <img class="img-preview img-preview-sm" id="preview-img"/>

                                                <label title="Upload image file" class="btn btn-primary">
                                                    <input onchange="readURL(this, 1)" accept="image/*" type="file"  id="member-img">
                                                </label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Fondo</label>
                                            <div class="col-sm-10">
                                                <h4>Preview</h4>
                                                <img class="img-preview img-preview-sm" id="preview-bg"/>

                                                <label title="Upload image file" class="btn btn-primary">
                                                    <input onchange="readURL(this, 2)" accept="image/*" type="file"  id="member-bg">
                                                </label>
                                            </div>
                                        </div>

                                        <div class="form-group"><label class="col-sm-2 control-label">Descricion</label>
                                            <div class="col-sm-10"><input type="text" class="form-control" id="descripcion"></div>
                                        </div>

                                        <div class="form-group"><label class="col-sm-2 control-label">Facebook</label>
                                            <div class="col-sm-10"><input type="text" class="form-control" id="facebook"></div>
                                        </div>

                                        <div class="form-group"><label class="col-sm-2 control-label">Twitter</label>
                                            <div class="col-sm-10"><input type="text" class="form-control" id="twitter"></div>
                                        </div>

                                        <div class="form-group"><label class="col-sm-2 control-label">Linkedin</label>
                                            <div class="col-sm-10"><input type="text" class="form-control" id="linkedin"></div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-4">
                                                <button class="btn btn-primary" id="save" >Guardar</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php include('footer.php'); ?>

                    </div>
                </div>


            </div>
            
        </div>

        <?php include('scripts.php'); ?>

        <script>
            function readURL(input, i) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        if(i==1){
                            $('#preview-img').attr('src', e.target.result);
                        }else{
                            $('#preview-bg').attr('src', e.target.result);
                        }

                    };

                    reader.readAsDataURL(input.files[0]);
                }
            }

            $(document).on("click", "#save", function(){
                var nombre = $("#nombre").val();
                var descripcion = $("#descripcion").val();
                var facebook = $("#facebook").val();
                var twitter = $("#twitter").val();
                var linkedin = $("#linkedin").val();

                var msg = "";

                if(nombre==null || nombre.trim()==""){
                    msg += 'Debe ingresar un nombre.<br>';
                    return;
                }
                if(descripcion==null || descripcion.trim()==""){
                    msg += 'Debe ingresar una descripcion.<br>';
                    return;
                }
                if(facebook==null || facebook.trim()=="") {
                    msg += 'Debe ingresar facebook.<br>';
                    return;
                }
                if(msg.trim()!=""){
                    toastr.warning(msg, 'Validación');
                    return;
                }

                $.ajax({
                    url: "team-add-save.php",
                    method: "POST",
                    data: {nombre: nombre, descripcion: descripcion, facebook: facebook, twitter: twitter, linkedin: linkedin},
                    success: function(id){
                        var file_data = $("#member-img").prop('files')[0];
                        var form_data = new FormData();
                        form_data.append('file', file_data);

                        $.ajax({
                            url: 'team-add-save-img.php?id='+id+'&code=1',
                            dataType: 'text',
                            cache: false,
                            contentType: false,
                            processData: false,
                            data: form_data,
                            type: 'POST',
                            success: function(result){
                                var file_data = $("#member-bg").prop('files')[0];
                                var form_data = new FormData();
                                form_data.append('file', file_data);

                                $.ajax({
                                    url: 'team-add-save-img.php?id='+id+'&code=2',
                                    dataType: 'text',
                                    cache: false,
                                    contentType: false,
                                    processData: false,
                                    data: form_data,
                                    type: 'POST',
                                    success: function(result){
                                        window.location.href = "team.php";
                                    }
                                });
                            }
                        });
                    }
                });
            });
        </script>

    </body>
</html>
