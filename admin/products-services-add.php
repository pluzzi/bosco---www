<?php
    include('isLogin.php');

    $page_id = 5;
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include('head.php'); ?>
    </head>

    <body>
        <div id="wrapper">
            <?php include('side-menu.php'); ?>

            <div id="page-wrapper" class="gray-bg dashbard-1">
                <?php include('top-menu.php'); ?>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper wrapper-content">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Nuevo Producto o Servicio</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="ibox-content">
                                    <div class="form-horizontal" >
                                        <div class="form-group"><label class="col-sm-2 control-label">Descripcion</label>
                                            <div class="col-sm-10"><input type="text" class="form-control" id="descripcion"></div>
                                        </div>

                                        <div class="form-group"><label class="col-sm-2 control-label">Icono</label>
                                            <div class="col-sm-10"><input type="text" class="form-control" id="icono"></div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Imagen</label>
                                            <div class="col-sm-10">
                                                <h4>Preview</h4>
                                                <img class="img-preview img-preview-sm" id="preview"/>

                                                <label title="Upload image file" for="inputImage" class="btn btn-primary">
                                                    <input onchange="readURL(this);" accept="image/*" type="file"  id="producto-img">
                                                </label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-4">
                                                <button class="btn btn-primary" id="save" >Guardar</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php include('footer.php'); ?>

                    </div>
                </div>

                <?php include('chat.php'); ?>

            </div>
            
        </div>

        <?php include('modal-iconos.php'); ?>

        <?php include('scripts.php'); ?>

        <script>
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#preview').attr('src', e.target.result);
                    };

                    reader.readAsDataURL(input.files[0]);
                }
            }

            $(document).ready(function() {

            });

            $(document).on("click", "#save", function(){
                var descripcion = $("#descripcion").val();
                var icono = $("#icono").val();
                var msg = "";
                
                if(descripcion==null || descripcion.trim()==""){
                    msg += 'Debe ingresar una descripcion.<br>';
                }
                if(icono==null || icono.trim()==""){
                    msg += 'Debe ingresar un icono.';
                }
                if(msg!=""){
                    toastr.warning(msg,'Validación');
                    return;
                }

                $.ajax({
                    url: "products-services-add-save.php",
                    method: "POST",
                    data: {descripcion: descripcion, icono: icono},
                    success: function(id){
                        var file_data = $("#producto-img").prop('files')[0];   
                        var form_data = new FormData();                  
                        form_data.append('file', file_data);

                        $.ajax({
                            url: 'products-services-add-save-img.php?id='+id,
                            dataType: 'text',
                            cache: false,
                            contentType: false,
                            processData: false,
                            data: form_data,                         
                            type: 'POST',
                            success: function(result){
                                window.location.href = "products-services.php";
                            }
                        });
                    }
                });
            });

            $(document).on("click", "#icono", function(){
                console.log("show");
                $('#modal-iconos').modal('show');
            });

            $(document).on("click", ".fa", function(){
                $("#icono").val("fa " + $(this).attr('class').split(' ')[3]);
                $('#modal-iconos').modal('hide');
                console.log("hide");
            });

        </script>

    </body>
</html>
