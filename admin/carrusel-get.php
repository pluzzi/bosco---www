<?php
    session_start();

    include('config/database-config.php');
        
    $sql = "select id, img, texto from carrusel";

    $result = mysqli_query($conn, $sql);

    while ($row = mysqli_fetch_assoc($result)) {

        if($row['img']!=null){
            $image = $row['img'];
            $im = new Imagick();
            $im->readimageblob($image);
            $im->thumbnailImage(200,100,true);
            $output = $im->getimageblob();
        }else{
            $output = null;
        }
        
        echo '<tr>
                <td>'.$row['id'] .'</td>
                <td><img alt="image" class="img-profile-size" src="data:image/jpeg;base64,'.base64_encode( $output ).'" />
                <td>'.$row['texto'] .'</td>
                <td>
                    <button id="edit" class="btn btn-primary btn-sm" data-id="'.$row['id'].'">
                        <i class="fa fa-edit"></i>
                    </button>
                    <button id="delete" class="btn btn-primary btn-sm" data-id="'.$row['id'].'">
                        <i class="fa fa-minus-circle"></i>
                    </button>
                </td>
            </tr>';
    }
?>
