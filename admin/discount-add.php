<?php
    include('isLogin.php');

    $page_id = 15;
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include('head.php'); ?>
    </head>

    <body>
        <div id="wrapper">
            <?php include('side-menu.php'); ?>

            <div id="page-wrapper" class="gray-bg dashbard-1">
                <?php include('top-menu.php'); ?>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper wrapper-content">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Agregar Descuento</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="ibox-content">
                                    <div class="form-horizontal" >
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Producto</label>
                                            <div class="col-sm-10">
                                                <select class="form-control" id="producto">
                                                    <option data-id="-1">Seleccionar</option>
                                                    <?php
                                                        include('config/database-config.php');

                                                        $sql = "select id, titulo from productos";

                                                        $result = mysqli_query($conn, $sql);

                                                        while ($row = mysqli_fetch_assoc($result)) {
                                                            echo '<option data-id="'.$row['id'].'">'.$row['titulo'].'</option>';
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Usuario</label>
                                            <div class="col-sm-10">
                                                <select class="form-control" id="usuario">
                                                    <option data-id="-1">Seleccionar</option>
                                                    <?php
                                                        include('config/database-config.php');

                                                        $sql = "select id, nombre, apellido from usuarios_ec";

                                                        $result = mysqli_query($conn, $sql);

                                                        while ($row = mysqli_fetch_assoc($result)) {
                                                            echo '<option data-id="'.$row['id'].'">'.$row['nombre'].', '.$row['apellido'].'</option>';
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group"><label class="col-sm-2 control-label">Descuento % [0 - 100]</label>
                                            <div class="col-sm-10"><input type="text" class="form-control" id="descuento"></div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-4">
                                                <button class="btn btn-primary" id="save" >Guardar</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php include('footer.php'); ?>

                    </div>
                </div>

                <?php include('chat.php'); ?>

            </div>
            
        </div>

        <?php include('scripts.php'); ?>

        <script>
            $(document).on("click", "#save", function(){
                var producto = $('#producto').find(':selected').data('id');
                var usuario = $('#usuario').find(':selected').data('id');
                var descuento = $('#descuento').val();

                var msg = "";

                $.ajax({
                    url: "discount-add-save.php",
                    method: "POST",
                    data: {
                        producto: producto,
                        usuario: usuario,
                        descuento: descuento
                    },
                    success: function(id){
                        window.location.href = "discount.php";
                    }
                });
            });
        </script>

    </body>
</html>
