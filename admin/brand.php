<?php
    include('isLogin.php');

    $page_id = 13;
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include('head.php'); ?>
    </head>

    <body>
        <div id="wrapper">
            <?php include('side-menu.php'); ?>

            <div id="page-wrapper" class="gray-bg dashbard-1">
                <?php include('top-menu.php'); ?>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper wrapper-content">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Marcas</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="ibox-content">
                                    <button id="add" class="btn btn-primary btn-sm">
                                        <i class="fa fa-plus-square"></i>
                                    </button>
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                                            <thead>
                                                <tr>
                                                    <th>Id</th>
                                                    <th>Código</th>
                                                    <th>Descripción</th>
                                                    <th>Logo</th>
                                                    <th>Acciones</th>
                                                </tr>
                                            </thead>
                                            <tbody id="results">
                                                
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <?php include('footer.php'); ?>

                    </div>
                </div>


            </div>
            
        </div>

        <?php include('scripts.php'); ?>

        <script>
            function getBrands(){
                $.ajax({
                    url: "brand-get.php",
                    method: "POST",
                    success: function(results){
                        $("#results").html(results);
                    }
                })
            }

            $(document).ready(function() {
                getBrands();
            });

            $(document).on("click", "#delete", function(){
                var id = $(this).data("id");

                $.ajax({
                    url: "brand-delete.php",
                    method: "POST",
                    data: {id: id},
                    success: function(results){
                        getBrands();
                    }
                })
            })

            $(document).on("click", "#add", function(){
                window.location.href = "brand-add.php";
            })

            $(document).on("click", "#edit", function(){
                var id = $(this).data("id");
                window.location.href = "brand-edit.php?id="+id;
            })
        </script>

    </body>
</html>
