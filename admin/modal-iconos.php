<style>
    .zoom{
        zoom: 2;
        margin: 5px;
    }
</style>
<div class="modal inmodal fade" id="modal-iconos" tabindex="-1" role="dialog"  aria-hidden="true" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Iconocs</h4>
                <small class="font-bold">Seleccione el icono que desea.</small>
            </div>

            <div class="modal-body" style="overflow-y: scroll; height: 300px !important;" >
                
                <div>
                    <h3> New Icons in 4.7.0 </h3>

                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-address-book" aria-hidden="true"></i> address-book</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-address-book-o" aria-hidden="true"></i> address-book-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-address-card" aria-hidden="true"></i> address-card</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-address-card-o" aria-hidden="true"></i> address-card-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-bandcamp" aria-hidden="true"></i> bandcamp</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-bath" aria-hidden="true"></i> bath</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-bathtub" aria-hidden="true"></i> bathtub <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-drivers-license" aria-hidden="true"></i> drivers-license <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-drivers-license-o" aria-hidden="true"></i> drivers-license-o <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-eercast" aria-hidden="true"></i> eercast</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-envelope-open" aria-hidden="true"></i> envelope-open</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-envelope-open-o" aria-hidden="true"></i> envelope-open-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-etsy" aria-hidden="true"></i> etsy</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-free-code-camp" aria-hidden="true"></i> free-code-camp</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-grav" aria-hidden="true"></i> grav</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-handshake-o" aria-hidden="true"></i> handshake-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-id-badge" aria-hidden="true"></i> id-badge</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-id-card" aria-hidden="true"></i> id-card</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-id-card-o" aria-hidden="true"></i> id-card-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-imdb" aria-hidden="true"></i> imdb</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-linode" aria-hidden="true"></i> linode</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-meetup" aria-hidden="true"></i> meetup</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-microchip" aria-hidden="true"></i> microchip</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-podcast" aria-hidden="true"></i> podcast</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-quora" aria-hidden="true"></i> quora</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-ravelry" aria-hidden="true"></i> ravelry</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-s15" aria-hidden="true"></i> s15 <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-shower" aria-hidden="true"></i> shower</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-snowflake-o" aria-hidden="true"></i> snowflake-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-superpowers" aria-hidden="true"></i> superpowers</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-telegram" aria-hidden="true"></i> telegram</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-thermometer" aria-hidden="true"></i> thermometer <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-thermometer-0" aria-hidden="true"></i> thermometer-0 <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-thermometer-1" aria-hidden="true"></i> thermometer-1 <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-thermometer-2" aria-hidden="true"></i> thermometer-2 <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-thermometer-3" aria-hidden="true"></i> thermometer-3 <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-thermometer-4" aria-hidden="true"></i> thermometer-4 <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-thermometer-empty" aria-hidden="true"></i> thermometer-empty</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-thermometer-full" aria-hidden="true"></i> thermometer-full</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-thermometer-half" aria-hidden="true"></i> thermometer-half</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-thermometer-quarter" aria-hidden="true"></i> thermometer-quarter</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-thermometer-three-quarters" aria-hidden="true"></i> thermometer-three-quarters</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-times-rectangle" aria-hidden="true"></i> times-rectangle <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-times-rectangle-o" aria-hidden="true"></i> times-rectangle-o <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-user-circle" aria-hidden="true"></i> user-circle</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-user-circle-o" aria-hidden="true"></i> user-circle-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-user-o" aria-hidden="true"></i> user-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-vcard" aria-hidden="true"></i> vcard <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-vcard-o" aria-hidden="true"></i> vcard-o <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-window-close" aria-hidden="true"></i> window-close</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-window-close-o" aria-hidden="true"></i> window-close-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-window-maximize" aria-hidden="true"></i> window-maximize</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-window-minimize" aria-hidden="true"></i> window-minimize</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-window-restore" aria-hidden="true"></i> window-restore</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-wpexplorer" aria-hidden="true"></i> wpexplorer</div>

                    <div class="clearfix">

                </div>
                

                <div>
                    <h3> New Icons in 4.3.0 </h3>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-angellist"></i> zoom fa-angellist</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-area-chart"></i> zoom fa-area-chart</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-at"></i> zoom fa-at</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-bell-slash"></i> zoom fa-bell-slash</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-bell-slash-o"></i> zoom fa-bell-slash-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-bicycle"></i> zoom fa-bicycle</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-binoculars"></i> zoom fa-binoculars</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-birthday-cake"></i> zoom fa-birthday-cake</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-bus"></i> zoom fa-bus</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-calculator"></i> zoom fa-calculator</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-cc"></i> zoom fa-cc</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-cc-amex"></i> zoom fa-cc-amex</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-cc-discover"></i> zoom fa-cc-discover</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-cc-mastercard"></i> zoom fa-cc-mastercard</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-cc-paypal"></i> zoom fa-cc-paypal</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-cc-stripe"></i> zoom fa-cc-stripe</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-cc-visa"></i> zoom fa-cc-visa</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-copyright"></i> zoom fa-copyright</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-eyedropper"></i> zoom fa-eyedropper</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-futbol-o"></i> zoom fa-futbol-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-google-wallet"></i> zoom fa-google-wallet</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-ils"></i> zoom fa-ils</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-ioxhost"></i> zoom fa-ioxhost</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-lastfm"></i> zoom fa-lastfm</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-lastfm-square"></i> zoom fa-lastfm-square</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-line-chart"></i> zoom fa-line-chart</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-meanpath"></i> zoom fa-meanpath</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-newspaper-o"></i> zoom fa-newspaper-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-paint-brush"></i> zoom fa-paint-brush</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-paypal"></i> zoom fa-paypal</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-pie-chart"></i> zoom fa-pie-chart</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-plug"></i> zoom fa-plug</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-shekel"></i> zoom fa-shekel <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-sheqel"></i> zoom fa-sheqel <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-slideshare"></i> zoom fa-slideshare</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-soccer-ball-o"></i> zoom fa-soccer-ball-o <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-toggle-off"></i> zoom fa-toggle-off</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-toggle-on"></i> zoom fa-toggle-on</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-trash"></i> zoom fa-trash</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-tty"></i> zoom fa-tty</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-twitch"></i> zoom fa-twitch</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-wifi"></i> zoom fa-wifi</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-yelp"></i> zoom fa-yelp</div>

                    <div class="clearfix"></div>
                </div>

                <div>
                    <h3> New Icons in 4.1.0 </h3>

                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-rub"></i> zoom fa-rub</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-ruble"></i> zoom fa-ruble <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-rouble"></i> zoom fa-rouble <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-pagelines"></i> zoom fa-pagelines</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-stack-exchange"></i> zoom fa-stack-exchange</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-arrow-circle-o-right"></i> zoom fa-arrow-circle-o-right</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-arrow-circle-o-left"></i> zoom fa-arrow-circle-o-left</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-caret-square-o-left"></i> zoom fa-caret-square-o-left</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-toggle-left"></i> zoom fa-toggle-left <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-dot-circle-o"></i> zoom fa-dot-circle-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-wheelchair"></i> zoom fa-wheelchair</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-vimeo-square"></i> zoom fa-vimeo-square</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-try"></i> zoom fa-try</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-turkish-lira"></i> zoom fa-turkish-lira <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-plus-square-o"></i> zoom fa-plus-square-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-automobile"></i> zoom fa-automobile <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-bank"></i> zoom fa-bank <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-behance"></i> zoom fa-behance</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-behance-square"></i> zoom fa-behance-square</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-bomb"></i> zoom fa-bomb</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-building"></i> zoom fa-building</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-cab"></i> zoom fa-cab <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-car"></i> zoom fa-car</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-child"></i> zoom fa-child</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-circle-o-notch"></i> zoom fa-circle-o-notch</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-circle-thin"></i> zoom fa-circle-thin</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-codepen"></i> zoom fa-codepen</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-cube"></i> zoom fa-cube</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-cubes"></i> zoom fa-cubes</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-database"></i> zoom fa-database</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-delicious"></i> zoom fa-delicious</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-deviantart"></i> zoom fa-deviantart</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-digg"></i> zoom fa-digg</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-drupal"></i> zoom fa-drupal</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-empire"></i> zoom fa-empire</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-envelope-square"></i> zoom fa-envelope-square</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-fax"></i> zoom fa-fax</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-file-archive-o"></i> zoom fa-file-archive-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-file-audio-o"></i> zoom fa-file-audio-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-file-code-o"></i> zoom fa-file-code-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-file-excel-o"></i> zoom fa-file-excel-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-file-image-o"></i> zoom fa-file-image-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-file-movie-o"></i> zoom fa-file-movie-o <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-file-pdf-o"></i> zoom fa-file-pdf-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-file-photo-o"></i> zoom fa-file-photo-o <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-file-picture-o"></i> zoom fa-file-picture-o <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-file-powerpoint-o"></i> zoom fa-file-powerpoint-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-file-sound-o"></i> zoom fa-file-sound-o <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-file-video-o"></i> zoom fa-file-video-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-file-word-o"></i> zoom fa-file-word-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-file-zip-o"></i> zoom fa-file-zip-o <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-ge"></i> zoom fa-ge <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-git"></i> zoom fa-git</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-git-square"></i> zoom fa-git-square</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-google"></i> zoom fa-google</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-graduation-cap"></i> zoom fa-graduation-cap</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-hacker-news"></i> zoom fa-hacker-news</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-header"></i> zoom fa-header</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-history"></i> zoom fa-history</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-institution"></i> zoom fa-institution <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-joomla"></i> zoom fa-joomla</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-jsfiddle"></i> zoom fa-jsfiddle</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-language"></i> zoom fa-language</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-life-bouy"></i> zoom fa-life-bouy <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-life-ring"></i> zoom fa-life-ring</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-life-saver"></i> zoom fa-life-saver <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-mortar-board"></i> zoom fa-mortar-board <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-openid"></i> zoom fa-openid</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-paper-plane"></i> zoom fa-paper-plane</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-paper-plane-o"></i> zoom fa-paper-plane-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-paragraph"></i> zoom fa-paragraph</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-paw"></i> zoom fa-paw</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-pied-piper"></i> zoom fa-pied-piper</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-pied-piper-alt"></i> zoom fa-pied-piper-alt</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-pied-piper-square"></i> zoom fa-pied-piper-square <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-qq"></i> zoom fa-qq</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-ra"></i> zoom fa-ra <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-rebel"></i> zoom fa-rebel</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-recycle"></i> zoom fa-recycle</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-reddit"></i> zoom fa-reddit</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-reddit-square"></i> zoom fa-reddit-square</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-send"></i> zoom fa-send <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-send-o"></i> zoom fa-send-o <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-share-alt"></i> zoom fa-share-alt</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-share-alt-square"></i> zoom fa-share-alt-square</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-slack"></i> zoom fa-slack</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-sliders"></i> zoom fa-sliders</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-soundcloud"></i> zoom fa-soundcloud</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-space-shuttle"></i> zoom fa-space-shuttle</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-spoon"></i> zoom fa-spoon</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-spotify"></i> zoom fa-spotify</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-steam"></i> zoom fa-steam</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-steam-square"></i> zoom fa-steam-square</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-stumbleupon"></i> zoom fa-stumbleupon</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-stumbleupon-circle"></i> zoom fa-stumbleupon-circle</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-support"></i> zoom fa-support <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-taxi"></i> zoom fa-taxi</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-tencent-weibo"></i> zoom fa-tencent-weibo</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-tree"></i> zoom fa-tree</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-university"></i> zoom fa-university</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-vine"></i> zoom fa-vine</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-wechat"></i> zoom fa-wechat <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-weixin"></i> zoom fa-weixin</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-wordpress"></i> zoom fa-wordpress</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-yahoo"></i> zoom fa-yahoo</div>

                    <div class="clearfix"></div>
                </div>

                <div>
                    <h3>Web Application Icons</h3>

                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-adjust"></i> zoom fa-adjust</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-anchor"></i> zoom fa-anchor</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-archive"></i> zoom fa-archive</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-arrows"></i> zoom fa-arrows</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-arrows-h"></i> zoom fa-arrows-h</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-arrows-v"></i> zoom fa-arrows-v</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-asterisk"></i> zoom fa-asterisk</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-ban"></i> zoom fa-ban</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-bar-chart-o"></i> zoom fa-bar-chart-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-barcode"></i> zoom fa-barcode</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-bars"></i> zoom fa-bars</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-beer"></i> zoom fa-beer</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-bell"></i> zoom fa-bell</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-bell-o"></i> zoom fa-bell-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-bolt"></i> zoom fa-bolt</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-book"></i> zoom fa-book</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-bookmark"></i> zoom fa-bookmark</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-bookmark-o"></i> zoom fa-bookmark-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-briefcase"></i> zoom fa-briefcase</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-bug"></i> zoom fa-bug</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-building-o"></i> zoom fa-building-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-bullhorn"></i> zoom fa-bullhorn</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-bullseye"></i> zoom fa-bullseye</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-calendar"></i> zoom fa-calendar</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-calendar-o"></i> zoom fa-calendar-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-camera"></i> zoom fa-camera</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-camera-retro"></i> zoom fa-camera-retro</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-caret-square-o-down"></i> zoom fa-caret-square-o-down</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-caret-square-o-left"></i> zoom fa-caret-square-o-left</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-caret-square-o-right"></i> zoom fa-caret-square-o-right</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-caret-square-o-up"></i> zoom fa-caret-square-o-up</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-certificate"></i> zoom fa-certificate</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-check"></i> zoom fa-check</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-check-circle"></i> zoom fa-check-circle</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-check-circle-o"></i> zoom fa-check-circle-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-check-square"></i> zoom fa-check-square</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-check-square-o"></i> zoom fa-check-square-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-circle"></i> zoom fa-circle</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-circle-o"></i> zoom fa-circle-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-clock-o"></i> zoom fa-clock-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-cloud"></i> zoom fa-cloud</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-cloud-download"></i> zoom fa-cloud-download</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-cloud-upload"></i> zoom fa-cloud-upload</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-code"></i> zoom fa-code</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-code-fork"></i> zoom fa-code-fork</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-coffee"></i> zoom fa-coffee</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-cog"></i> zoom fa-cog</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-cogs"></i> zoom fa-cogs</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-comment"></i> zoom fa-comment</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-comment-o"></i> zoom fa-comment-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-comments"></i> zoom fa-comments</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-comments-o"></i> zoom fa-comments-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-compass"></i> zoom fa-compass</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-credit-card"></i> zoom fa-credit-card</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-crop"></i> zoom fa-crop</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-crosshairs"></i> zoom fa-crosshairs</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-cutlery"></i> zoom fa-cutlery</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-dashboard"></i> zoom fa-dashboard <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-desktop"></i> zoom fa-desktop</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-dot-circle-o"></i> zoom fa-dot-circle-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-download"></i> zoom fa-download</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-edit"></i> zoom fa-edit <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-ellipsis-h"></i> zoom fa-ellipsis-h</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-ellipsis-v"></i> zoom fa-ellipsis-v</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-envelope"></i> zoom fa-envelope</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-envelope-o"></i> zoom fa-envelope-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-eraser"></i> zoom fa-eraser</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-exchange"></i> zoom fa-exchange</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-exclamation"></i> zoom fa-exclamation</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-exclamation-circle"></i> zoom fa-exclamation-circle</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-exclamation-triangle"></i> zoom fa-exclamation-triangle</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-external-link"></i> zoom fa-external-link</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-external-link-square"></i> zoom fa-external-link-square</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-eye"></i> zoom fa-eye</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-eye-slash"></i> zoom fa-eye-slash</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-female"></i> zoom fa-female</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-fighter-jet"></i> zoom fa-fighter-jet</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-film"></i> zoom fa-film</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-filter"></i> zoom fa-filter</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-fire"></i> zoom fa-fire</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-fire-extinguisher"></i> zoom fa-fire-extinguisher</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-flag"></i> zoom fa-flag</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-flag-checkered"></i> zoom fa-flag-checkered</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-flag-o"></i> zoom fa-flag-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-flash"></i> zoom fa-flash <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-flask"></i> zoom fa-flask</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-folder"></i> zoom fa-folder</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-folder-o"></i> zoom fa-folder-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-folder-open"></i> zoom fa-folder-open</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-folder-open-o"></i> zoom fa-folder-open-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-frown-o"></i> zoom fa-frown-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-gamepad"></i> zoom fa-gamepad</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-gavel"></i> zoom fa-gavel</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-gear"></i> zoom fa-gear <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-gears"></i> zoom fa-gears <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-gift"></i> zoom fa-gift</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-glass"></i> zoom fa-glass</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-globe"></i> zoom fa-globe</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-group"></i> zoom fa-group <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-hdd-o"></i> zoom fa-hdd-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-headphones"></i> zoom fa-headphones</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-heart"></i> zoom fa-heart</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-heart-o"></i> zoom fa-heart-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-home"></i> zoom fa-home</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-inbox"></i> zoom fa-inbox</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-info"></i> zoom fa-info</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-info-circle"></i> zoom fa-info-circle</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-key"></i> zoom fa-key</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-keyboard-o"></i> zoom fa-keyboard-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-laptop"></i> zoom fa-laptop</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-leaf"></i> zoom fa-leaf</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-legal"></i> zoom fa-legal <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-lemon-o"></i> zoom fa-lemon-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-level-down"></i> zoom fa-level-down</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-level-up"></i> zoom fa-level-up</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-lightbulb-o"></i> zoom fa-lightbulb-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-location-arrow"></i> zoom fa-location-arrow</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-lock"></i> zoom fa-lock</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-magic"></i> zoom fa-magic</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-magnet"></i> zoom fa-magnet</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-mail-forward"></i> zoom fa-mail-forward <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-mail-reply"></i> zoom fa-mail-reply <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-mail-reply-all"></i> zoom fa-mail-reply-all</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-male"></i> zoom fa-male</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-map-marker"></i> zoom fa-map-marker</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-meh-o"></i> zoom fa-meh-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-microphone"></i> zoom fa-microphone</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-microphone-slash"></i> zoom fa-microphone-slash</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-minus"></i> zoom fa-minus</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-minus-circle"></i> zoom fa-minus-circle</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-minus-square"></i> zoom fa-minus-square</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-minus-square-o"></i> zoom fa-minus-square-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-mobile"></i> zoom fa-mobile</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-mobile-phone"></i> zoom fa-mobile-phone <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-money"></i> zoom fa-money</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-moon-o"></i> zoom fa-moon-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-music"></i> zoom fa-music</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-pencil"></i> zoom fa-pencil</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-pencil-square"></i> zoom fa-pencil-square</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-pencil-square-o"></i> zoom fa-pencil-square-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-phone"></i> zoom fa-phone</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-phone-square"></i> zoom fa-phone-square</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-picture-o"></i> zoom fa-picture-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-plane"></i> zoom fa-plane</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-plus"></i> zoom fa-plus</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-plus-circle"></i> zoom fa-plus-circle</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-plus-square"></i> zoom fa-plus-square</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-plus-square-o"></i> zoom fa-plus-square-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-power-off"></i> zoom fa-power-off</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-print"></i> zoom fa-print</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-puzzle-piece"></i> zoom fa-puzzle-piece</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-qrcode"></i> zoom fa-qrcode</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-question"></i> zoom fa-question</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-question-circle"></i> zoom fa-question-circle</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-quote-left"></i> zoom fa-quote-left</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-quote-right"></i> zoom fa-quote-right</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-random"></i> zoom fa-random</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-refresh"></i> zoom fa-refresh</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-reply"></i> zoom fa-reply</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-reply-all"></i> zoom fa-reply-all</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-retweet"></i> zoom fa-retweet</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-road"></i> zoom fa-road</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-rocket"></i> zoom fa-rocket</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-rss"></i> zoom fa-rss</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-rss-square"></i> zoom fa-rss-square</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-search"></i> zoom fa-search</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-search-minus"></i> zoom fa-search-minus</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-search-plus"></i> zoom fa-search-plus</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-share"></i> zoom fa-share</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-share-square"></i> zoom fa-share-square</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-share-square-o"></i> zoom fa-share-square-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-shield"></i> zoom fa-shield</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-shopping-cart"></i> zoom fa-shopping-cart</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-sign-in"></i> zoom fa-sign-in</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-sign-out"></i> zoom fa-sign-out</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-signal"></i> zoom fa-signal</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-sitemap"></i> zoom fa-sitemap</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-smile-o"></i> zoom fa-smile-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-sort"></i> zoom fa-sort</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-sort-alpha-asc"></i> zoom fa-sort-alpha-asc</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-sort-alpha-desc"></i> zoom fa-sort-alpha-desc</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-sort-amount-asc"></i> zoom fa-sort-amount-asc</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-sort-amount-desc"></i> zoom fa-sort-amount-desc</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-sort-asc"></i> zoom fa-sort-asc</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-sort-desc"></i> zoom fa-sort-desc</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-sort-down"></i> zoom fa-sort-down <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-sort-numeric-asc"></i> zoom fa-sort-numeric-asc</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-sort-numeric-desc"></i> zoom fa-sort-numeric-desc</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-sort-up"></i> zoom fa-sort-up <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-spinner"></i> zoom fa-spinner</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-square"></i> zoom fa-square</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-square-o"></i> zoom fa-square-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-star"></i> zoom fa-star</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-star-half"></i> zoom fa-star-half</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-star-half-empty"></i> zoom fa-star-half-empty <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-star-half-full"></i> zoom fa-star-half-full <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-star-half-o"></i> zoom fa-star-half-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-star-o"></i> zoom fa-star-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-subscript"></i> zoom fa-subscript</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-suitcase"></i> zoom fa-suitcase</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-sun-o"></i> zoom fa-sun-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-superscript"></i> zoom fa-superscript</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-tablet"></i> zoom fa-tablet</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-tachometer"></i> zoom fa-tachometer</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-tag"></i> zoom fa-tag</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-tags"></i> zoom fa-tags</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-tasks"></i> zoom fa-tasks</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-terminal"></i> zoom fa-terminal</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-thumb-tack"></i> zoom fa-thumb-tack</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-thumbs-down"></i> zoom fa-thumbs-down</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-thumbs-o-down"></i> zoom fa-thumbs-o-down</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-thumbs-o-up"></i> zoom fa-thumbs-o-up</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-thumbs-up"></i> zoom fa-thumbs-up</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-ticket"></i> zoom fa-ticket</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-times"></i> zoom fa-times</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-times-circle"></i> zoom fa-times-circle</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-times-circle-o"></i> zoom fa-times-circle-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-tint"></i> zoom fa-tint</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-toggle-down"></i> zoom fa-toggle-down <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-toggle-left"></i> zoom fa-toggle-left <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-toggle-right"></i> zoom fa-toggle-right <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-toggle-up"></i> zoom fa-toggle-up <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-trash-o"></i> zoom fa-trash-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-trophy"></i> zoom fa-trophy</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-truck"></i> zoom fa-truck</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-umbrella"></i> zoom fa-umbrella</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-unlock"></i> zoom fa-unlock</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-unlock-alt"></i> zoom fa-unlock-alt</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-unsorted"></i> zoom fa-unsorted <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-upload"></i> zoom fa-upload</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-user"></i> zoom fa-user</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-users"></i> zoom fa-users</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-video-camera"></i> zoom fa-video-camera</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-volume-down"></i> zoom fa-volume-down</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-volume-off"></i> zoom fa-volume-off</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-volume-up"></i> zoom fa-volume-up</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-warning"></i> zoom fa-warning <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-wheelchair"></i> zoom fa-wheelchair</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-wrench"></i> zoom fa-wrench</div>
                    <div class="clearfix"></div>
                </div>

                <div>
                    <h3>Form Control Icons</h3>

                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-check-square"></i> zoom fa-check-square</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-check-square-o"></i> zoom fa-check-square-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-circle"></i> zoom fa-circle</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-circle-o"></i> zoom fa-circle-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-dot-circle-o"></i> zoom fa-dot-circle-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-minus-square"></i> zoom fa-minus-square</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-minus-square-o"></i> zoom fa-minus-square-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-plus-square"></i> zoom fa-plus-square</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-plus-square-o"></i> zoom fa-plus-square-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-square"></i> zoom fa-square</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-square-o"></i> zoom fa-square-o</div>
                    <div class="clearfix"></div>
                </div>

                <div>
                    <h3>Currency Icons</h3>

                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-bitcoin"></i> zoom fa-bitcoin <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-btc"></i> zoom fa-btc</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-cny"></i> zoom fa-cny <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-dollar"></i> zoom fa-dollar <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-eur"></i> zoom fa-eur</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-euro"></i> zoom fa-euro <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-gbp"></i> zoom fa-gbp</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-inr"></i> zoom fa-inr</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-jpy"></i> zoom fa-jpy</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-krw"></i> zoom fa-krw</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-money"></i> zoom fa-money</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-rmb"></i> zoom fa-rmb <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-rouble"></i> zoom fa-rouble <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-rub"></i> zoom fa-rub</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-ruble"></i> zoom fa-ruble <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-rupee"></i> zoom fa-rupee <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-try"></i> zoom fa-try</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-turkish-lira"></i> zoom fa-turkish-lira <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-usd"></i> zoom fa-usd</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-won"></i> zoom fa-won <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-yen"></i> zoom fa-yen <span class="text-muted">(alias)</span></div>
                    <div class="clearfix"></div>
                </div>

                <div>
                    <h3>Text Editor Icons</h3>

                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-align-center"></i> zoom fa-align-center</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-align-justify"></i> zoom fa-align-justify</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-align-left"></i> zoom fa-align-left</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-align-right"></i> zoom fa-align-right</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-bold"></i> zoom fa-bold</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-chain"></i> zoom fa-chain <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-chain-broken"></i> zoom fa-chain-broken</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-clipboard"></i> zoom fa-clipboard</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-columns"></i> zoom fa-columns</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-copy"></i> zoom fa-copy <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-cut"></i> zoom fa-cut <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-dedent"></i> zoom fa-dedent <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-eraser"></i> zoom fa-eraser</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-file"></i> zoom fa-file</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-file-o"></i> zoom fa-file-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-file-text"></i> zoom fa-file-text</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-file-text-o"></i> zoom fa-file-text-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-files-o"></i> zoom fa-files-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-floppy-o"></i> zoom fa-floppy-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-font"></i> zoom fa-font</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-indent"></i> zoom fa-indent</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-italic"></i> zoom fa-italic</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-link"></i> zoom fa-link</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-list"></i> zoom fa-list</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-list-alt"></i> zoom fa-list-alt</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-list-ol"></i> zoom fa-list-ol</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-list-ul"></i> zoom fa-list-ul</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-outdent"></i> zoom fa-outdent</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-paperclip"></i> zoom fa-paperclip</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-paste"></i> zoom fa-paste <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-repeat"></i> zoom fa-repeat</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-rotate-left"></i> zoom fa-rotate-left <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-rotate-right"></i> zoom fa-rotate-right <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-save"></i> zoom fa-save <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-scissors"></i> zoom fa-scissors</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-strikethrough"></i> zoom fa-strikethrough</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-table"></i> zoom fa-table</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-text-height"></i> zoom fa-text-height</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-text-width"></i> zoom fa-text-width</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-th"></i> zoom fa-th</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-th-large"></i> zoom fa-th-large</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-th-list"></i> zoom fa-th-list</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-underline"></i> zoom fa-underline</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-undo"></i> zoom fa-undo</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-unlink"></i> zoom fa-unlink <span class="text-muted">(alias)</span></div>
                    <div class="clearfix"></div>
                </div>

                <div>
                    <h3>Directional Icons</h3>

                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-angle-double-down"></i> zoom fa-angle-double-down</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-angle-double-left"></i> zoom fa-angle-double-left</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-angle-double-right"></i> zoom fa-angle-double-right</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-angle-double-up"></i> zoom fa-angle-double-up</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-angle-down"></i> zoom fa-angle-down</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-angle-left"></i> zoom fa-angle-left</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-angle-right"></i> zoom fa-angle-right</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-angle-up"></i> zoom fa-angle-up</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-arrow-circle-down"></i> zoom fa-arrow-circle-down</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-arrow-circle-left"></i> zoom fa-arrow-circle-left</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-arrow-circle-o-down"></i> zoom fa-arrow-circle-o-down</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-arrow-circle-o-left"></i> zoom fa-arrow-circle-o-left</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-arrow-circle-o-right"></i> zoom fa-arrow-circle-o-right</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-arrow-circle-o-up"></i> zoom fa-arrow-circle-o-up</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-arrow-circle-right"></i> zoom fa-arrow-circle-right</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-arrow-circle-up"></i> zoom fa-arrow-circle-up</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-arrow-down"></i> zoom fa-arrow-down</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-arrow-left"></i> zoom fa-arrow-left</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-arrow-right"></i> zoom fa-arrow-right</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-arrow-up"></i> zoom fa-arrow-up</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-arrows"></i> zoom fa-arrows</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-arrows-alt"></i> zoom fa-arrows-alt</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-arrows-h"></i> zoom fa-arrows-h</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-arrows-v"></i> zoom fa-arrows-v</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-caret-down"></i> zoom fa-caret-down</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-caret-left"></i> zoom fa-caret-left</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-caret-right"></i> zoom fa-caret-right</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-caret-square-o-down"></i> zoom fa-caret-square-o-down</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-caret-square-o-left"></i> zoom fa-caret-square-o-left</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-caret-square-o-right"></i> zoom fa-caret-square-o-right</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-caret-square-o-up"></i> zoom fa-caret-square-o-up</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-caret-up"></i> zoom fa-caret-up</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-chevron-circle-down"></i> zoom fa-chevron-circle-down</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-chevron-circle-left"></i> zoom fa-chevron-circle-left</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-chevron-circle-right"></i> zoom fa-chevron-circle-right</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-chevron-circle-up"></i> zoom fa-chevron-circle-up</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-chevron-down"></i> zoom fa-chevron-down</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-chevron-left"></i> zoom fa-chevron-left</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-chevron-right"></i> zoom fa-chevron-right</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-chevron-up"></i> zoom fa-chevron-up</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-hand-o-down"></i> zoom fa-hand-o-down</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-hand-o-left"></i> zoom fa-hand-o-left</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-hand-o-right"></i> zoom fa-hand-o-right</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-hand-o-up"></i> zoom fa-hand-o-up</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-long-arrow-down"></i> zoom fa-long-arrow-down</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-long-arrow-left"></i> zoom fa-long-arrow-left</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-long-arrow-right"></i> zoom fa-long-arrow-right</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-long-arrow-up"></i> zoom fa-long-arrow-up</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-toggle-down"></i> zoom fa-toggle-down <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-toggle-left"></i> zoom fa-toggle-left <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-toggle-right"></i> zoom fa-toggle-right <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-toggle-up"></i> zoom fa-toggle-up <span class="text-muted">(alias)</span></div>
                    <div class="clearfix"></div>
                </div>

                <div>
                    <h3>Video Player Icons</h3>

                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-arrows-alt"></i> zoom fa-arrows-alt</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-backward"></i> zoom fa-backward</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-compress"></i> zoom fa-compress</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-eject"></i> zoom fa-eject</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-expand"></i> zoom fa-expand</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-fast-backward"></i> zoom fa-fast-backward</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-fast-forward"></i> zoom fa-fast-forward</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-forward"></i> zoom fa-forward</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-pause"></i> zoom fa-pause</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-play"></i> zoom fa-play</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-play-circle"></i> zoom fa-play-circle</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-play-circle-o"></i> zoom fa-play-circle-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-step-backward"></i> zoom fa-step-backward</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-step-forward"></i> zoom fa-step-forward</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-stop"></i> zoom fa-stop</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-youtube-play"></i> zoom fa-youtube-play</div>
                    <div class="clearfix"></div>
                </div>

                <div>
                    <h3>Brand Icons</h3>

                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-adn"></i> zoom fa-adn</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-android"></i> zoom fa-android</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-apple"></i> zoom fa-apple</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-bitbucket"></i> zoom fa-bitbucket</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-bitbucket-square"></i> zoom fa-bitbucket-square</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-bitcoin"></i> zoom fa-bitcoin <span class="text-muted">(alias)</span></div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-btc"></i> zoom fa-btc</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-css3"></i> zoom fa-css3</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-dribbble"></i> zoom fa-dribbble</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-dropbox"></i> zoom fa-dropbox</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-facebook"></i> zoom fa-facebook</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-facebook-square"></i> zoom fa-facebook-square</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-flickr"></i> zoom fa-flickr</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-foursquare"></i> zoom fa-foursquare</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-github"></i> zoom fa-github</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-github-alt"></i> zoom fa-github-alt</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-github-square"></i> zoom fa-github-square</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-gittip"></i> zoom fa-gittip</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-google-plus"></i> zoom fa-google-plus</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-google-plus-square"></i> zoom fa-google-plus-square</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-html5"></i> zoom fa-html5</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-instagram"></i> zoom fa-instagram</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-linkedin"></i> zoom fa-linkedin</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-linkedin-square"></i> zoom fa-linkedin-square</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-linux"></i> zoom fa-linux</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-maxcdn"></i> zoom fa-maxcdn</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-pagelines"></i> zoom fa-pagelines</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-pinterest"></i> zoom fa-pinterest</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-pinterest-square"></i> zoom fa-pinterest-square</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-renren"></i> zoom fa-renren</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-skype"></i> zoom fa-skype</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-stack-exchange"></i> zoom fa-stack-exchange</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-stack-overflow"></i> zoom fa-stack-overflow</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-trello"></i> zoom fa-trello</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-tumblr"></i> zoom fa-tumblr</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-tumblr-square"></i> zoom fa-tumblr-square</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-twitter"></i> zoom fa-twitter</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-twitter-square"></i> zoom fa-twitter-square</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-vimeo-square"></i> zoom fa-vimeo-square</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-vk"></i> zoom fa-vk</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-weibo"></i> zoom fa-weibo</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-windows"></i> zoom fa-windows</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-xing"></i> zoom fa-xing</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-xing-square"></i> zoom fa-xing-square</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-youtube"></i> zoom fa-youtube</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-youtube-play"></i> zoom fa-youtube-play</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-youtube-square"></i> zoom fa-youtube-square</div>
                    <div class="clearfix"></div>
                </div>

                <div>
                    <h3>Medical Icons</h3>

                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-ambulance"></i> zoom fa-ambulance</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-h-square"></i> zoom fa-h-square</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-hospital-o"></i> zoom fa-hospital-o</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-medkit"></i> zoom fa-medkit</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-plus-square"></i> zoom fa-plus-square</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-stethoscope"></i> zoom fa-stethoscope</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-user-md"></i> zoom fa-user-md</div>
                    <div class="infont col-md-3 col-sm-4"><i class="zoom fa zoom fa-wheelchair"></i> zoom fa-wheelchair</div>
                    <div class="clearfix"></div>
                </div>

            </div>
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Select</button>
        </div>
    </div>
</div>