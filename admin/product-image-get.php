<?php
    session_start();

    include('config/database-config.php');
        
    $sql = "select * from  imagenes_producto where producto=".$_GET['id'];

    $result = mysqli_query($conn, $sql) or die (mysqli_error());

    while ($row = mysqli_fetch_assoc($result)) {
        if($row['img']!=null){
            $image = $row['img'];
            $im = new Imagick();
            $im->readimageblob($image);
            $im->thumbnailImage(300,150,true);
            $output = $im->getimageblob();
        }
        
        echo '<tr>
                <td>'.$row['id'] .'</td>
                <td><img alt="image" class="img-profile-size" src="data:image/jpeg;base64,'.base64_encode( $output ).'" />
                <td>
                    <button id="delete" class="btn btn-primary btn-sm" data-id="'.$row['id'].'">
                        <i class="fa fa-minus-circle"></i>
                    </button>
                </td>
            </tr>';
    }
?>
