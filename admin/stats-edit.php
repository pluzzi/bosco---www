<?php
    include('isLogin.php');

    $page_id = 7;

    include('config/database-config.php');
    $sql = "select * from estadisticas where id=".$_GET['id'];
    $results = $conn->query($sql);
    $stat = mysqli_fetch_assoc($results);

?>
<!DOCTYPE html>
<html>
    <head>
        <?php include('head.php'); ?>
    </head>

    <body>
        <div id="wrapper">
            <?php include('side-menu.php'); ?>

            <div id="page-wrapper" class="gray-bg dashbard-1">
                <?php include('top-menu.php'); ?>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper wrapper-content">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Editar estadística</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="ibox-content">
                                    <div class="form-horizontal" >
                                        <div class="form-group"><label class="col-sm-2 control-label">Titulo</label>
                                            <div class="col-sm-10"><input type="text" class="form-control" id="titulo" value="<?php echo $stat['titulo']; ?>"></div>
                                        </div>

                                        <div class="form-group"><label class="col-sm-2 control-label">Descripcion</label>
                                            <div class="col-sm-10"><input type="text" class="form-control" id="descripcion" value="<?php echo $stat['descripcion']; ?>"></div>
                                        </div>

                                        <div class="form-group"><label class="col-sm-2 control-label">Porcentaje</label>
                                            <div class="col-sm-10"><input type="text" class="form-control" id="porcentaje" value="<?php echo $stat['porcentaje']; ?>"></div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php include('footer.php'); ?>

                    </div>
                </div>


            </div>
            
        </div>

        <?php include('scripts.php'); ?>

        <script>
            function update_data(value, column){
                if(value==null || value.trim()==""){
                    toastr.warning('Debe ingresar el campo '+column,'Validación');
                    return;
                }
                
                var $_GET = <?php echo json_encode($_GET); ?>;
                
                $.ajax({
                    url: "stats-edit-save.php",
                    method: "POST",
                    data: {valor: value, columna: column, id: $_GET['id']},
                    success: function(results){}
                });
            }

            $(document).on("blur", "#titulo", function(){
                var value = $(this).val();
                update_data(value, "titulo");
            });
            $(document).on("blur", "#descripcion", function(){
                var value = $(this).val();
                update_data(value, "descripcion");
            });
            $(document).on("blur", "#porcentaje", function(){
                var value = $(this).val();
                update_data(value, "porcentaje");
            });

        </script>

    </body>
</html>