<?php
    session_start();

    include('config/database-config.php');
        
    $sql = "select * from equipo";

    $result = mysqli_query($conn, $sql);

    while ($row = mysqli_fetch_assoc($result)) {
        if($row['foto']!=null){
            $image = $row['foto'];
            $im = new Imagick();
            $im->readimageblob($image);
            $im->thumbnailImage(300,150,true);
            $output = $im->getimageblob();
        }else{
            $output = null;
        }

        echo '<tr>
                <td>'.$row['id'] .'</td>
                <td>'.$row['nombre'] .'</td>
                <td><img alt="image" class="img-profile-size" src="data:image/jpeg;base64,'.base64_encode( $output ).'" /></td>
                <td>
                    <button id="edit" class="btn btn-primary btn-sm" data-id="'.$row['id'].'">
                        <i class="fa fa-edit"></i>
                    </button>
                    <button id="delete" class="btn btn-primary btn-sm" data-id="'.$row['id'].'">
                        <i class="fa fa-minus-circle"></i>
                    </button>
                </td>
            </tr>';
    }
?>
