<?php
    $hash = $_GET['id'];

    include('config/database-config.php');

    $sql = "select * from usuarios where hash_password='".$hash."'" ;
    $result = $conn->query($sql);
    
    if($result->num_rows == 0){
        header("Location: index.php");
    }

    $row = mysqli_fetch_assoc($result)

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Movi Bar | Password</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>
                <h1 class="logo-name">MB</h1>
            </div>
            <h3>Bienvenido a Movi Bar</h3>
            <p>Ingrese su contraseña para el panel de administrador de Movi Bar</p>
            
            <form class="m-t" role="form" action="users-password-change.php?id=<?php echo $row['id']; ?>" method="post">
                <div class="form-group">
                    <input type="password" class="form-control" placeholder="Contraseña" required="" name="password">
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">Setear</button>
            </form>
            <p class="m-t"> <small>Movi Bar diseñado por Patricio Luzzi &copy; 2019</small> </p>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
