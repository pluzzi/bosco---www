<?php
    include('isLogin.php');

    $page_id = 14;

    $id_producto = $_GET['id'];

?>
<!DOCTYPE html>
<html>
    <head>
        <?php include('head.php'); ?>
    </head>

    <body>
        <div id="wrapper">
            <?php include('side-menu.php'); ?>

            <div id="page-wrapper" class="gray-bg dashbard-1">
                <?php include('top-menu.php'); ?>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper wrapper-content">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Imágenes de Prodcutos de tienda</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="ibox-content">
                                    <button id="add" class="btn btn-primary btn-sm">
                                        <i class="fa fa-plus-square"></i>
                                    </button>
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                                            <thead>
                                                <tr>
                                                    <th>Id</th>
                                                    <th>Imagen</th>
                                                    <th>Acciones</th>
                                                </tr>
                                            </thead>
                                            <tbody id="results">
                                                
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <?php include('footer.php'); ?>

                    </div>
                </div>

                <?php include('chat.php'); ?>

            </div>
            
        </div>

        <?php include('scripts.php'); ?>

        <script>
            function getImages(){
                var id = <?php echo $id_producto; ?>;

                $.ajax({
                    url: "product-image-get.php?id="+id,
                    method: "POST",
                    success: function(results){
                        $("#results").html(results);
                    }
                })
            }

            $(document).ready(function() {
                getImages();
            });

            $(document).on("click", "#delete", function(){
                var id = $(this).data("id");

                $.ajax({
                    url: "product-image-delete.php",
                    method: "POST",
                    data: {id: id},
                    success: function(results){
                        getImages();
                    }
                })
            });

            $(document).on("click", "#add", function(){
                var id = <?php echo $id_producto; ?>;
                window.location.href = "product-image-add.php?id="+id;
            });

        </script>

    </body>
</html>
