<?php
    include('isLogin.php');

    $page_id = 14;
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include('head.php'); ?>
    </head>

    <body>
        <div id="wrapper">
            <?php include('side-menu.php'); ?>

            <div id="page-wrapper" class="gray-bg dashbard-1">
                <?php include('top-menu.php'); ?>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper wrapper-content">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Agregar Producto de Tienda</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="ibox-content">
                                    <div class="form-horizontal" >
                                        <div class="form-group"><label class="col-sm-2 control-label">Titulo</label>
                                            <div class="col-sm-10"><input type="text" class="form-control" id="titulo"></div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Descripción</label>
                                            <div class="col-sm-10">
                                                <div class="summernote">

                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Mostrar stock</label>
                                            <div class="col-sm-10">
                                                <div class="i-checks"><input type="checkbox" value="" id="mostrar_stock" ></div>
                                            </div>
                                        </div>

                                        <div class="form-group"><label class="col-sm-2 control-label">Stock</label>
                                            <div class="col-sm-10"><input type="text" class="form-control" id="stock"></div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Marca</label>
                                            <div class="col-sm-10">
                                                <select class="form-control" id="marca">
                                                    <?php
                                                    include('config/database-config.php');

                                                    $sql = "select id, descripcion from marcas";

                                                    $result = mysqli_query($conn, $sql);

                                                    while ($row = mysqli_fetch_assoc($result)) {
                                                        echo '<option data-id="'.$row['id'].'">'.$row['descripcion'].'</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Más vendido</label>
                                            <div class="col-sm-10">
                                                <div class="i-checks"><input type="checkbox" value="" id="mas_vendido" ></div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Envío gratis</label>
                                            <div class="col-sm-10">
                                                <div class="i-checks"><input type="checkbox" value="" id="envio_gratis" ></div>
                                            </div>
                                        </div>

                                        <div class="form-group"><label class="col-sm-2 control-label">Precio</label>
                                            <div class="col-sm-10"><input type="text" class="form-control" id="precio"></div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-4">
                                                <button class="btn btn-primary" id="save" >Guardar</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php include('footer.php'); ?>

                    </div>
                </div>

                <?php include('chat.php'); ?>

            </div>
            
        </div>

        <?php include('scripts.php'); ?>

        <script>
            $(document).ready(function() {
                $('.summernote').summernote({
                    height: 200
                });
            });

            $(document).on("click", "#save", function(){
                var titulo = $("#titulo").val();
                var descripcion = $('.summernote').summernote('code');
                var mostrar_stock = $('#mostrar_stock').is(":checked")
                var stock = $("#stock").val();
                var marca = $('#marca').find(':selected').data('id');
                var mas_vendido = $('#mas_vendido').is(":checked")
                var envio_gratis = $('#envio_gratis').is(":checked")
                var precio = $('#precio').val();

                console.log();

                var msg = "";

                if(titulo==null || titulo.trim()==""){
                    msg += 'Debe ingresar un título.<br>';
                }
                if(descripcion==null || descripcion.trim()==""){
                    msg += 'Debe ingresar una descripcion.<br>';
                }
                if(stock==null || stock.trim()==""){
                    msg += 'Debe ingresar número de stock.<br>';
                }
                if(precio==null || precio.trim()==""){
                    msg += 'Debe ingresar un precio.<br>';
                }
                if(msg!=""){
                    toastr.warning(msg,'Validación');
                    return;
                }

                $.ajax({
                    url: "product-add-save.php",
                    method: "POST",
                    data: {
                        titulo: titulo,
                        descripcion: descripcion,
                        mostrar_stock: mostrar_stock,
                        stock: stock,
                        marca: marca,
                        mas_vendido: mas_vendido,
                        envio_gratis: envio_gratis,
                        precio: precio
                    },
                    success: function(id){
                        window.location.href = "product.php";
                    }
                });
            });
        </script>

    </body>
</html>
