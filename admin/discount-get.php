<?php
    session_start();

    include('config/database-config.php');
        
    $sql = "select 
            d.id,
            p.titulo as producto,
            concat(u.nombre , ', ', u.apellido) as usuario,
            descuento
            from descuentos_producto d 
            left join productos p on p.id = d.producto
            left join usuarios_ec u on u.id = d.usuario";

    $result = mysqli_query($conn, $sql) or die (mysqli_error());

    while ($row = mysqli_fetch_assoc($result)) {
        echo '<tr>
                <td>'.$row['id'] .'</td>
                <td>'.$row['producto'] .'</td>
                <td>'.$row['usuario'] .'</td>
                <td>'.$row['descuento'] .'</td>
                <td>
                    <button id="edit" class="btn btn-primary btn-sm" data-id="'.$row['id'].'">
                        <i class="fa fa-edit"></i>
                    </button>
                    <button id="delete" class="btn btn-primary btn-sm" data-id="'.$row['id'].'">
                        <i class="fa fa-minus-circle"></i>
                    </button>
                </td>
            </tr>';
    }
?>
