<?php
    include('isLogin.php');

    $page_id = 11;
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include('head.php'); ?>
    </head>

    <body>
        <div id="wrapper">
            <?php include('side-menu.php'); ?>

            <div id="page-wrapper" class="gray-bg dashbard-1">
                <?php include('top-menu.php'); ?>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper wrapper-content">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Nuevo Usuario</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="ibox-content">
                                    <div class="form-horizontal" >
                                        <div class="form-group"><label class="col-sm-2 control-label">Comentario</label>
                                            <div class="col-sm-10"><input type="text" class="form-control" id="comentario"></div>
                                        </div>

                                        <div class="form-group"><label class="col-sm-2 control-label">Nombre</label>
                                            <div class="col-sm-10"><input type="text" class="form-control" id="nombre"></div>
                                        </div>

                                        <div class="form-group"><label class="col-sm-2 control-label">Roll</label>
                                            <div class="col-sm-10"><input type="text" class="form-control" id="roll"></div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-4">
                                                <button class="btn btn-primary" id="save" >Guardar</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php include('footer.php'); ?>

                    </div>
                </div>


            </div>
            
        </div>

        <?php include('scripts.php'); ?>

        <script>
            $(document).on("click", "#save", function(){
                var comentario = $("#comentario").val();
                var nombre = $("#nombre").val();
                var roll = $("#roll").val();

                var msg = "";

                if(comentario==null || comentario.trim()==""){
                    msg += 'Debe ingresar un comentario.';
                }
                if(nombre==null || nombre.trim()==""){
                    msg += 'Debe ingresar un nombre.';
                }
                if(roll==null || roll.trim()==""){
                    msg += 'Debe ingresar un roll.';
                }
                if(msg.trim()!=""){
                    toastr.warning(msg,'Validación');
                    return;
                }

                $.ajax({
                    url: "customer-comment-add-save.php",
                    method: "POST",
                    data: {comentario: comentario, nombre: nombre, roll: roll},
                    success: function(result){
                        window.location.href = "customer-comment.php";
                    }
                });
            });
        </script>

    </body>
</html>
