<?php
    include('isLogin.php');

    $page_id = 5;
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include('head.php'); ?>
    </head>

    <body>
        <div id="wrapper">
            <?php include('side-menu.php'); ?>

            <div id="page-wrapper" class="gray-bg dashbard-1">
                <?php include('top-menu.php'); ?>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper wrapper-content">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Detalle de Producto o Servicio</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="ibox-content">
                                    <button id="add" class="btn btn-primary btn-sm">
                                        <i class="fa fa-plus-square"></i>
                                    </button>
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                                            <thead>
                                                <tr>
                                                    <th>Id</th>
                                                    <th>Descripcion</th>
                                                    <th>Acciones</th>
                                                </tr>
                                            </thead>
                                            <tbody id="results">
                                                
                                            </tbody>
                                        </table>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-4">
                                            <button class="btn btn-primary" id="volver" >Volver</button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <?php include('footer.php'); ?>

                    </div>
                </div>

                <?php include('chat.php'); ?>

            </div>
            
        </div>

        <?php include('scripts.php'); ?>

        <script>
            function getProductos(){
                var $_GET = <?php echo json_encode($_GET); ?>;

                $.ajax({
                    url: "products-services-details-get.php",
                    method: "POST",
                    data: {id: $_GET['id']},
                    success: function(results){
                        $("#results").html(results);
                    }
                })
            }

            $(document).ready(function() {
                getProductos();
            });

            $(document).on("click", "#delete", function(){
                var id = $(this).data("id");

                $.ajax({
                    url: "products-services-details-delete.php",
                    method: "POST",
                    data: {id: id},
                    success: function(results){
                        getProductos();
                    }
                })
            });

            $(document).on("click", "#add", function(){
                var $_GET = <?php echo json_encode($_GET); ?>;
                window.location.href = "products-services-details-add.php?prod="+$_GET['id'];
            });

            $(document).on("click", "#edit", function(){
                var id = $(this).data("id");
                window.location.href = "products-services-details-edit.php?id="+id;
            });

            $(document).on("click", "#volver", function(){
                window.location.href = "products-services.php";
            });
        </script>

    </body>
</html>
