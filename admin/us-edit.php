<?php
    include('isLogin.php');

    $page_id = 3;

    include('config/database-config.php');
    $sql = "select * from nosotros where id=".$_GET['id'];
    $results = $conn->query($sql);
    $nosotros = mysqli_fetch_assoc($results);

?>
<!DOCTYPE html>
<html>
    <head>
        <?php include('head.php'); ?>
    </head>

    <body>
        <div id="wrapper">
            <?php include('side-menu.php'); ?>

            <div id="page-wrapper" class="gray-bg dashbard-1">
                <?php include('top-menu.php'); ?>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper wrapper-content">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Editar A que nos dedicamos</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>

                                <div class="ibox-content">
                                    <div class="form-horizontal" >
                                        <div class="form-group"><label class="col-sm-2 control-label">Titulo</label>
                                            <div class="col-sm-10"><input type="text" class="form-control" id="titulo" value="<?php echo $nosotros['titulo']; ?>"></div>
                                        </div>
                                        <div class="form-group"><label class="col-sm-2 control-label">Descripcion</label>
                                            <div class="col-sm-10"><input type="text" class="form-control" id="descripcion" value="<?php echo $nosotros['descripcion']; ?>"></div>
                                        </div>
                                        <div class="form-group"><label class="col-sm-2 control-label">Icono</label>
                                            <div class="col-sm-10"><input type="text" class="form-control" id="icono" value="<?php echo $nosotros['icono']; ?>"></div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php include('footer.php'); ?>

                    </div>
                </div>

                <?php include('chat.php'); ?>

            </div>
            
        </div>

        <?php include('modal-iconos.php'); ?>

        <?php include('scripts.php'); ?>

        <script>
            function update_data(value, column){
                if(value==null || value.trim()==""){
                    toastr.warning('Debe ingresar el campo '+column,'Validación');
                    return;
                }
                var $_GET = <?php echo json_encode($_GET); ?>;
                
                $.ajax({
                    url: "us-edit-save.php",
                    method: "POST",
                    data: {valor: value, columna: column, id: $_GET['id']},
                    success: function(results){}
                });
            }

            $(document).on("blur", "#titulo", function(){
                var value = $(this).val();
                update_data(value, "titulo");
            });
            $(document).on("blur", "#descripcion", function(){
                var value = $(this).val();
                update_data(value, "descripcion");
            });
            $(document).on("blur", "#icono", function(){
                var value = $(this).val();
                update_data(value, "icono");
            });

            $(document).on("click", "#icono", function(){
                console.log("show");
                $('#modal-iconos').modal('show');
            });

            $(document).on("click", ".fa", function(){
                $("#icono").val("fa " + $(this).attr('class').split(' ')[3]);
                $('#modal-iconos').modal('hide');
                update_data("fa " + $(this).attr('class').split(' ')[3], "icono");
                console.log("hide");
            });

        </script>

    </body>
</html>