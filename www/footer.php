
<aside id="footer-widgets">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-md-offset-1">
                <h3 class="footer-widget-title">Contacto</h3>
                <p>Bartolomé Mitre 1128 <br>
                    S2252 Gálvez, Santa Fe <br>
                    Tel.: +54 9 3404 639316 <br>
                    E-Mail: info@boscohnos.com.ar</p>
            </div>

            <div class="col-md-3 col-md-offset-1">
                <!-- Business Hours -->
                <h3 class="footer-widget-title">Horarios de atención</h3>
                <p>Lunes-Viernes: 7:00 – 20:00 <br>
                    Sabados: 7:00 – 12:00<br>
                    Domingos: Cerrado </p>

            </div>

            <div class="col-md-3 col-md-offset-1">
                <h3 class="footer-widget-title">Datos Fiscales</h3>
                <a href="http://qr.afip.gob.ar/?qr=mtbgOdC-ew9Z_-d6OSaOGA,," target="_F960AFIPInfo">
                    <img src="http://www.afip.gob.ar/images/f960/DATAWEB.jpg" border="0" style="width: 100px">
                </a>
            </div>

        </div> <!-- row -->
    </div> <!-- container -->
</aside> <!-- footer-widgets -->

<footer id="footer">
    <p>&copy; 2020 Designed by <a href="#">Patricio Luzzi</a>. All rights reserved.</p>
</footer>