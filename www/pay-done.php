<?php
    $id_page = 2;
    session_start();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('head.php'); ?>
</head>

<!-- Preloader -->
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>

<body>

    <div class="sb-site-container">
        <div class="boxed">
            <?php include('header.php'); ?>

            <?php include('menu.php'); ?>

            <header class="main-header">
                <div class="container">
                    <h1 class="page-title">Pago</h1>
                </div>
            </header>

            <div class="container">
                <div class="row">
                    <div class="col-md-9">
                        <?php
                            if(isset($_GET['status'])){
                                if($_GET['status']=='approved'){
                                    echo '<div class="alert alert-success">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                            <strong><i class="fa fa-check"></i> Pago realizado!</strong> El pago fue realizado exitosamenete, un vendedor se pondrá en contacto con Ud.
                                        </div>';
                                }else{
                                    echo '<div class="alert alert-danger">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                            <strong><i class="fa fa-times"></i> Error!</strong> El pago no se pudo realizar, intentelo nuevamente.
                                        </div>';
                                }

                            }

                        ?>

                    </div>

                </div> <!-- row -->
            </div> <!-- container -->

            <?php include('footer.php'); ?>

        </div> <!-- boxed -->
    </div> <!-- sb-site -->


    <div id="back-top">
        <a href="#header"><i class="fa fa-chevron-up"></i></a>
    </div>

    <?php include('scripts.php'); ?>

</body>

</html>
