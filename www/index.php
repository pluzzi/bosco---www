<?php
    $id_page = 1;
    session_start();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('head.php'); ?>
</head>

<!-- Preloader-->
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>

<body>

    <div class="sb-site-container">
        <div class="boxed">
            <?php include('header.php'); ?>

            <?php include('menu.php'); ?>

            <?php include('carousel.php'); ?>

            <div class="container">
                <?php include('about.php'); ?>

                <?php include('produts-servives.php'); ?>
            </div> <!-- container -->

            <?php //include('stats.php'); ?>

            <?php include('recent-product.php'); ?>

            <?php include('information.php'); ?>

            <?php //include('numbers.php'); ?>

            <?php //include('plans.php'); ?>

            <div class="container">
                <?php //include('team.php'); ?>

                <?php //include('clients.php'); ?>

            </div> <!-- container -->

            <?php include('brands.php'); ?>

            <?php include('footer.php'); ?>

        </div> <!-- boxed -->
    </div> <!-- sb-site -->

    <div class="btn-whatsapp">
        <a href="https://api.whatsapp.com/send?phone=543404639823" target="_blank">
            <img src="http://s2.accesoperu.com/logos/btn_whatsapp.png" alt="">
        </a>
    </div>

    <?php include('scripts.php'); ?>

</body>


</html>