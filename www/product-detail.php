<?php
    $id_page = 2;
    session_start();
?>

<?php
    include("config/database-config.php");

    $sql = "SELECT * FROM productos where id=".$_GET['id'];
    $results = mysqli_query($conn, $sql);
    $producto = mysqli_fetch_array($results);

    $sql = "SELECT codigo, dolar FROM marcas where id='".$producto['brand_id']."' limit 1";
    $marcas = mysqli_query($conn, $sql);
    $marca = mysqli_fetch_array($marcas);

    $sql = "select round(IFNULL(avg(puntos),0),1) as puntuacion from puntuaciones_productos where producto_id=".$producto['id'];
    $resultado = mysqli_query($conn, $sql);
    $puntuacion = mysqli_fetch_array($resultado);

    $stock = $producto['mostrar_stock']==1 ? $producto['stock'].' Unidades' : '';

    if(isset($_SESSION['id'])){
        $sql = "select descuento from descuentos_producto where producto=".$producto['id']." and usuario=".$_SESSION['id'];
        $descuentos = mysqli_query($conn, $sql);

        if(mysqli_num_rows($descuentos)==0){
            $precio = $producto['precio'] * $marca['dolar'];
            $precio_sin_descuento = "";
            $porcentaje_descuento = "";
        }else{
            $descuento = mysqli_fetch_array($descuentos);
            $precio = $producto['precio']*(1-$descuento['descuento']/100) * $marca['dolar'];
            $precio_sin_descuento = '<h3><span class="label label-info label-line-through">$ '.$producto['precio'] * $marca['dolar'].'</span>';
            $porcentaje_descuento = '<span class="label label-danger pull-right">-'.$descuento['descuento'].'%</span></h3>';
        }
    }else{
        $precio = $producto['precio'] * $marca['dolar'];
        $precio_sin_descuento = "";
        $porcentaje_descuento = "";
    }

    $envio_gratis = $producto['envio_gratis']==1 ? 'Envío gratis' : '';

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('head.php'); ?>
</head>

<!-- Preloader -->
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>

<body>

<div class="sb-site-container">
    <div class="boxed">
        <?php include('header.php'); ?>

        <?php include('menu.php'); ?>

        <header class="main-header">
            <div class="container">
                <h1 class="page-title">Detalle del Producto</h1>
            </div>
        </header>

        <div class="container">
            <h2 class="section-title no-margin-top"><?php echo $producto['titulo']; ?></h2>
            <div class="row">
                <div class="col-md-4">

                    <ul class="bxslider">
                        <?php
                            include("config/database-config.php");

                            $sql = "SELECT * FROM imagenes_producto where producto = ".$_GET['id'];
                            $results = mysqli_query($conn, $sql);

                            while($row = mysqli_fetch_array($results)){
                                echo '<li><img src="data:image/jpeg;base64,'.base64_encode( $row['img'] ).'" /></li>';
                            }
                        ?>

                    </ul>

                    <div id="bx-pager" >

                        <?php
                            include("config/database-config.php");

                            $sql = "SELECT * FROM imagenes_producto where producto = ".$_GET['id'];
                            $results = mysqli_query($conn, $sql);

                            $i = 0;

                            while($row = mysqli_fetch_array($results)){
                                if($row['img']!=null){
                                    $image = $row['img'];
                                    $im = new Imagick();
                                    $im->readimageblob($image);
                                    $im->thumbnailImage(100,80,true);
                                    $output = $im->getimageblob();
                                }
                                echo '<a data-slide-index="'.$i++.'" href="" ><img style="width: 100px; height: 80px;" src="data:image/jpeg;base64,'.base64_encode( $output ).'" /></a>';
                            }
                        ?>
                    </div>
                </div>
                <div class="col-md-5">
                    <h3 class="no-margin-top">Descripción</h3>
                    <?php echo $producto['descripcion']; ?>

                </div>
                <div class="col-md-3">
                    <div class="e-price">$ <span><?php echo $precio; ?></span></div>

                    <div class="ec-box-footer">
                        <?php echo $precio_sin_descuento; ?>
                        <?php echo $porcentaje_descuento; ?>
                    </div>

                    <div class="ec-box-footer" style="margin-bottom: 10px;" >
                        <span class="pull-right" style="margin-left: 10px"><?php echo $puntuacion['puntuacion']; ?> <i class="glyphicon glyphicon-star" style="color: #c5b319;"></i></span>
                        <span class="" ><?php echo $stock; ?></span>
                    </div>

                    <div class="ec-box-footer" style="margin-bottom: 10px">
                        <span style="color: #02c66c;"><?php echo $envio_gratis; ?></span>
                    </div>

                    <button onclick="agregar('<?php echo $producto['id']; ?>')" class="btn btn-ar btn-block btn-success"><i class="fa fa-shopping-cart"></i> Agregar</button>

                </div>
            </div>
        </div>

        <?php include('footer.php'); ?>

    </div> <!-- boxed -->
</div> <!-- sb-site -->


<div id="back-top">
    <a href="#header"><i class="fa fa-chevron-up"></i></a>
</div>

<?php include('scripts.php'); ?>

</body>

</html>
