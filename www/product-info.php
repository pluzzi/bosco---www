<?php
    session_start();
    include("config/database-config.php");

    $sql = "SELECT * FROM productos where id=".$_POST['id'];
    $results = mysqli_query($conn, $sql);
    $producto = mysqli_fetch_array($results);

    $sql = "SELECT * FROM imagenes_producto where producto='".$_POST['id']."' limit 1";
    $imagenes = mysqli_query($conn, $sql);
    $imagen = mysqli_fetch_array($imagenes);

    if($imagen['img']!=null){
        $image = $imagen['img'];
        $im = new Imagick();
        $im->readimageblob($image);
        $im->thumbnailImage(420,200,true);
        $output = $im->getimageblob();
    }

    $sql = "SELECT codigo, dolar FROM marcas where id='".$producto['brand_id']."' limit 1";
    $marcas = mysqli_query($conn, $sql);
    $marca = mysqli_fetch_array($marcas);

    $sql = "select round(IFNULL(avg(puntos),0),1) as puntuacion from puntuaciones_productos where producto_id=".$producto['id'];
    $resultado = mysqli_query($conn, $sql);
    $puntuacion = mysqli_fetch_array($resultado);

    $stock = $producto['mostrar_stock']==1 ? $producto['stock'].' Unidades' : '';

    if(isset($_SESSION['id'])){
        $sql = "select descuento from descuentos_producto where producto=".$producto['id']." and usuario=".$_SESSION['id'];
        $descuentos = mysqli_query($conn, $sql);

        if(mysqli_num_rows($descuentos)==0){
            $precio = $producto['precio'] * $marca['dolar'];
            $precio_sin_descuento = "";
            $porcentaje_descuento = "";
            $mostrar_descuento = "";
        }else{
            $descuento = mysqli_fetch_array($descuentos);
            $precio = $producto['precio']*(1-$descuento['descuento']/100) * $marca['dolar'];
            $precio_sin_descuento = '<h3><span class="label label-info label-line-through">$ '.$producto['precio'] * $marca['dolar'].'</span>';
            $porcentaje_descuento = '<span class="label label-danger pull-right">-'.$descuento['descuento'].'%</span></h3>';
            $mostrar_descuento = '<div class="ec-box-footer">'.$precio_sin_descuento.$porcentaje_descuento.'</div>';
        }
    }else{
        $precio = $producto['precio'] * $marca['dolar'];
        $precio_sin_descuento = "";
        $porcentaje_descuento = "";
        $mostrar_descuento = "";
    }

    $envio_gratis = $producto['envio_gratis']==1 ? '<div class="ec-box-footer" style="margin-bottom: 10px"><span style="color: #02c66c;">Envío gratis</span></div>' : '';


?>

<div class="col-lg-8">
    <strong>Producto:</strong> <?php echo $producto['titulo']; ?>
    <br>
    <p style="margin-top: 10px;" ><strong>Descripción:</strong></p>
    <?php echo $producto['descripcion']; ?>
</div>
<div class="col-lg-4">
    <p><?php echo '<img src="data:image/jpeg;base64,'.base64_encode( $output ).'" />'; ?></p>

    <div class="e-price">$ <span><?php echo $precio; ?></span></div>

    <?php echo $mostrar_descuento; ?>

    <div class="ec-box-footer" style="margin-bottom: 10px;" >
        <span class="pull-right" style="margin-left: 10px"><?php echo $puntuacion['puntuacion']; ?> <i class="glyphicon glyphicon-star" style="color: #c5b319;"></i></span>
        <span class="" ><?php echo $stock; ?></span>
    </div>

    <?php echo $envio_gratis; ?>

</div>




