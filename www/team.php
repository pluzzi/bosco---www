<section class="margin-top">
    <h2 class="right-line">Nuestro Equipo</h2>

    <?php
        include("config/database-config.php");

        $sql = "SELECT * FROM equipo";
        $results = mysqli_query($conn, $sql);
        $i = 1;

        while($row = mysqli_fetch_array($results)){
            if($i==1){
                echo '<div class="row">';
            }

            $image = $row['foto'];
            $im = new Imagick();
            $im->readimageblob($image);
            $im->thumbnailImage(150,150,true);
            $foto = $im->getimageblob();

            $image = $row['fondo'];
            $im = new Imagick();
            $im->readimageblob($image);
            $im->thumbnailImage(100,100,true);
            $fondo = $im->getimageblob();


            echo '<div class="col-sm-6 col-md-3">
                    <div class="panel panel-default panel-card wow fadeInRight animation-delay-2">
                        <div class="panel-heading">
                            <img src="data:image/jpeg;base64,'.base64_encode( $fondo ).'" />
                        </div>
                        <div class="panel-figure">
                            <img class="img-responsive img-circle" src="data:image/jpeg;base64,'.base64_encode( $foto ).'" />
                        </div>
                        <div class="panel-body text-center">
                            <h4 class="panel-header"><a href="#">@'.$row['nombre'].'</a></h4>
                            <small>'.$row['descripcion'].'</small>
                        </div>
                        <div class="panel-thumbnails">
                            <div class="row">
                                <div class="col-xs-4">
                                    <div class="thumbnail" style="text-align: center;">
                                        <a class="fa fa-facebook" style="zoom: 1.8" href="'.$row['facebook'].'" target="_blank"></a>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="thumbnail" style="text-align: center;">
                                        <a class="fa fa-twitter" style="zoom: 1.8" href="'.$row['twitter'].'" target="_blank"></a>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="thumbnail" style="text-align: center;">
                                        <a class="fa fa-linkedin" style="zoom: 1.8" href="'.$row['linkedin'].'" target="_blank"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>';

            if($i==4){
                echo '</div>';
                $i = 1;
            }else{
                $i++;
            }
        }

    ?>
</div>
