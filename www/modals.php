<div class="modal fade mb-8" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p id="body"></p>
            </div>
            <div class="modal-footer">
                <button id="btn" type="button" class="btn" data-dismiss="modal">Ok</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="shopModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title">Agregar Producto al Carro</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div id="body"></div>
                </div>

                <p>
                    <div class="form-group">
                        <label for="cantidad">Cantidad:</label>
                        <input type="number" step="1" min="1" value="1" class="form-control" id="cantidad">
                    </div>
                </p>


            </div>
            <div class="modal-footer">
                <button onclick="add(this)" id="add" data-id="" data-cantidad="" type="button" class="btn btn-success" data-dismiss="modal">Agregar</button>
                <button onclick="addGoToCart(this)" id="addGoToCart" data-id="" type="button" class="btn btn-success" data-dismiss="modal">Agregar y Pagar</button>
                <button id="cancel" type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>