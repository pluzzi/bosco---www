<?php
    $id_page = 3;
    session_start();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('head.php'); ?>
</head>

<!-- Preloader -->
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>

<body>

    <div class="sb-site-container">
        <div class="boxed">
            <?php include('header.php'); ?>

            <?php include('menu.php'); ?>

            <header class="main-header">
                <div class="container">
                    <h1 class="page-title">Nosotros</h1>
                </div>
            </header>

            <div class="container">
                <div class="row">
                    <div class="col-md-5">

                        <p>La empresa inicia sus actividades en el año 1989 por iniciativa de su dueño actual bajo el nombre de Electromecanica Bosco, dedicándose a la reparación y mantenimiento de motores eléctricos industriales.</p>

                        <p>Luego de varios años de crecimiento sostenido en el mercado logró afianzarse como un centro de servicios especializado en la reparación integral de todo tipo de motores eléctricos de corriente alterna y continua,  electrobombas centrifugas y sumergibles.</p>

                        <p>En el año 2011, la empresa fue designada como Asistente Técnico Autorizado (ATA) de la firma brasilera WEG EQUIPAMIENTOS ELECTRICOS. Esto le permitió, además, iniciar la comercialización de toda su línea de productos, incursionando en variadores electrónicos de velocidad, soft starters y controladores para motores eléctricos. </p>

                        <p>En el año 2013 la empresa continuó sus actividades agregando  Bosco Ingenieria  y siguió afianzándose en el mercado incorporando nuevos productos como electrobombas y reductores mecánicos.</p>

                        <p>En la actualidad, la empresa cuenta con una superficie cubierta de 600 m2, repartidos en oficinas, depósitos y talleres; donde un equipo de experimentados operarios realiza mediante materiales de alta calidad las tareas de reparación eléctrica y mecánica. Asegurando que la calidad original de los motores o equipos confiados a nuestra firma sea renovada eficientemente.</p>

                        <p>Nuestra mejor propuesta es que nuestros clientes se beneficien con la excelente combinación de larga trayectoria, amplio conocimiento y variado stock.</p>

                        <p>Somos una empresa Familiar.  Clientes, personal y proveedores, hacemos de ELECTROMECANICA e INGENIERIA BOSCO una gran familia que está dispuesta a seguir creciendo y abre sus puertas para todo aquel que quiera crecer con ella.</p>
                    </div>

                    <div class="col-md-4">
                        <img src="assets/img/about/bosco01.jpg" class="about-img">
                        <img src="assets/img/about/bosco02.jpg" class="about-img">
                        <img src="assets/img/about/bosco03.jpg" class="about-img">
                    </div>

                </div> <!-- row -->
            </div> <!-- container -->

            <?php include('footer.php'); ?>

        </div> <!-- boxed -->
    </div> <!-- sb-site -->


    <div id="back-top">
        <a href="#header"><i class="fa fa-chevron-up"></i></a>
    </div>

    <?php include('scripts.php'); ?>

</body>

</html>
