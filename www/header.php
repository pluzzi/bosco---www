<header id="header-full-top" class="hidden-xs header-full-dark">
    <div class="container" >
        <div class="header-full-title">
            <h1 class="animated fadeInRight" style="width: 250px; height: 60px">

            </h1>
        </div>

        
        <nav class="top-nav">
            <ul class="top-nav-social hidden-sm">
                <?php 
                    include("config/database-config.php");
                    
                    $sql = "SELECT * FROM configuracion WHERE nemonico = 'SOCIAL' ";
                    $socials = mysqli_query($conn, $sql);
                    $i = 6;

                    while($social = mysqli_fetch_array($socials)){
                        echo '<li><a target="_blank" href="'.$social["valor"].'" class="animated fadeIn animation-delay-6 '.$social["valor2"].'"><i class="fa fa-'.$social["valor2"].'"></i></a></li>';
                        $i++;
                    }
                        
                    mysqli_free_result($socials);
                    mysqli_close($conn);

                ?>
                
            </ul>

            <?php
                if(!isset($_SESSION['id'])){
                    echo '<div class="dropdown animated fadeInDown animation-delay-'.$i++.'">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-lock"></i> Ingresar</a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-login-box animated flipCenter">
                                    <h4>Datos</h4>
            
                                    <div class="form-group">
                                        <div class="input-group login-input">
                                            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                            <input type="text" class="form-control" placeholder="Email" id="login-email">
                                        </div>
                                        <br>
                                        <div class="input-group login-input">
                                            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                            <input type="password" class="form-control" placeholder="Password" id="login-password">
                                        </div>
                                        
                                        <button class="btn btn-ar btn-primary pull-right" id="ingresar">Ingresar</button>
                                        <div class="clearfix"></div>
                                    </div>
            
                            </div>
                        </div> <!-- dropdown -->';
                }
            ?>

            <?php
                if(!isset($_SESSION['id'])){
                    echo '<div class="dropdown animated fadeInDown animation-delay-'.$i++.'">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Registrarse</a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-login-box animated flipCenter">
                                <h4>Datos</h4>
            
                                <div class="form-group">
                                    <div class="input-group login-input">
                                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                        <input type="text" class="form-control" placeholder="Nombre" id="register-nombre">
                                    </div>
                                    <br>
                                    <div class="input-group login-input">
                                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                        <input type="text" class="form-control" placeholder="Apellido" id="register-apellido">
                                    </div>
                                    <br>
                                    <div class="input-group login-input">
                                        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                        <input type="text" class="form-control" placeholder="Email" id="register-email">
                                    </div>
                                    <br>
                                    <div class="input-group login-input">
                                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                        <input type="password" class="form-control" placeholder="Password" id="register-password">
                                    </div>
            
                                    <button class="btn btn-ar btn-primary pull-right" id="registrarse">Registrarse</button>
                                    <div class="clearfix"></div>
                                </div>
            
                            </div>
                        </div>';

                }
            ?>

            <div class="dropdown animated fadeInDown animation-delay-<?php echo $i++; ?>">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-search"></i></a>
                <div class="dropdown-menu dropdown-menu-right dropdown-search-box animated fadeInUp">
                    <form role="form">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Buscar...">
                            <span class="input-group-btn">
                                <button class="btn btn-ar btn-primary" type="button">Buscar!</button>
                            </span>
                        </div><!-- /input-group -->
                    </form>
                </div>
            </div> <!-- dropdown -->

            <?php
                if(isset($_SESSION['id'])){
                    echo '<ul class="top-nav-social hidden-sm">
                            <li>
                                <a href="cart.php" class="animated fadeIn animation-delay-'.$i++.'">
                                    <i class="fa fa-shopping-cart"></i>
                                </a
                            </li>
                            <span class="badge badge-warning" style="background-color: #f0ad4e !important;" id="compras"></span>
                        </ul>';
                }
            ?>

            <?php
            if(isset($_SESSION['id'])){
                echo '<ul class="top-nav-social hidden-sm">
                                <li><a class="animated fadeIn animation-delay-'.$i++.'">Bienvenid@ '.$_SESSION['nombre'].' '.$_SESSION['apellido'].'!</a></li>
                            </ul>';
            }
            ?>

            <?php
                if(isset($_SESSION['id'])){
                    echo '<ul class="top-nav-social hidden-sm">
                                <li><a href="logout.php" class="animated fadeIn animation-delay-'.$i.'"><i class="fa fa-power-off"></i></a></li>
                            </ul>';
                }
            ?>

        </nav>
    </div> <!-- container -->
</header> <!-- header-full -->

<?php include('modals.php'); ?>