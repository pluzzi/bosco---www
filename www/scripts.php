<!-- Scripts -->
<!-- Compiled in vendors.js -->
<!--
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/jquery.cookie.js"></script>
<script src="assets/js/imagesloaded.pkgd.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/bootstrap-switch.min.js"></script>
<script src="assets/js/wow.min.js"></script>
<script src="assets/js/slidebars.min.js"></script>
<script src="assets/js/jquery.bxslider.min.js"></script>
<script src="assets/js/holder.js"></script>
<script src="assets/js/buttons.js"></script>
<script src="assets/js/jquery.mixitup.min.js"></script>
<script src="assets/js/circles.min.js"></script>
<script src="assets/js/masonry.pkgd.min.js"></script>
<script src="assets/js/jquery.matchHeight-min.js"></script>
-->

<script src="assets/js/vendors.js"></script>

<!--<script type="text/javascript" src="assets/js/jquery.themepunch.tools.min.js?rev=5.0"></script>
<script type="text/javascript" src="assets/js/jquery.themepunch.revolution.min.js?rev=5.0"></script>-->


<!-- Syntaxhighlighter -->
<script src="assets/js/syntaxhighlighter/shCore.js"></script>
<script src="assets/js/syntaxhighlighter/shBrushXml.js"></script>
<script src="assets/js/syntaxhighlighter/shBrushJScript.js"></script>

<script src="assets/js/DropdownHover.js"></script>
<script src="assets/js/app.js"></script>
<script src="assets/js/holder.js"></script>

<script src="assets/js/commerce.js"></script>
<script src="assets/js/e-commerce_product.js"></script>

<script src="assets/js/index.js"></script>

<script src="assets/js/shop.js"></script>

<script>
    $('#ingresar').click(function () {
        var email = $('#login-email').val();
        var password = $('#login-password').val();

        $.ajax({
            url: "authenticate.php",
            method: "POST",
            data: {email: email, password: password},
            success: function(result){
                window.location.href = "index.php";
            },
            error: function(xhr, status, error) {
                var err = eval("(" + xhr.responseText + ")");
                console.log(err.message);
                $('#infoModal #title').html('Usuario incorrecto.');
                $('#infoModal #body').html(err.message);
                $('#infoModal #btn').removeClass('btn-success');
                $('#infoModal #btn').addClass('btn-danger');
                $('#infoModal').modal('show');
            }
        });
    });

    $('#registrarse').click(function () {
        var nombre = $('#register-nombre').val();
        var apellido = $('#register-apellido').val();
        var email = $('#register-email').val();
        var password = $('#register-password').val();

        $.ajax({
            url: "register.php",
            method: "POST",
            data: {nombre: nombre, apellido: apellido, email: email, password: password},
            success: function(result){

            },
            error: function(xhr, status, error) {
                var err = eval("(" + xhr.responseText + ")");
                console.log(err.message);
                $('#infoModal #title').html('Error al registrarse.');
                $('#infoModal #body').html(err.message);
                $('#infoModal #btn').removeClass('btn-success');
                $('#infoModal #btn').addClass('btn-danger');
                $('#infoModal').modal('show');
            }
        });
    });

    $(document).ready(function () {
        getCompras();
    });

</script>





