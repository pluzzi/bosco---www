<section class="wrap-primary-color margin-bottom">
    <div class="container">
        <div class="row">
            <?php 
                include("config/database-config.php");
                
                $sql = "SELECT * FROM estadisticas order by id desc limit 4";
                $results = mysqli_query($conn, $sql) or die (mysqli_error());
                $i=1;

                while($row = mysqli_fetch_array($results)){
                    echo '<div class="col-sm-6 col-md-3">
                            <div class="text-center">
                                <div class="circle" id="circles-'.$i++.'"></div>
                                <h4 class="text-center">'.$row['titulo'].'</h4>
                                <p class="small-font">'.$row['descripcion'].'</p>
                            </div>
                        </div>';
                }
                mysqli_free_result($results);
                mysqli_close($conn);

            ?>
    </div>
</section>

<script>
    var chart4 = 30;
</script>