function getProducts(){
    $.ajax({
        url: "products-get.php",
        method: "POST",
        success: function(results){
            $("#results").html(results);
        }
    });
}

function getSum(){
    $.ajax({
        url: "products-get-sum.php",
        method: "POST",
        success: function(results){
            if(results!=''){
                var sum = '$ <span>'+results+'</span>';
            }else{
                var sum = '$ <span>0</span>';
            }

            $("#sum").html(sum);
        }
    });
}

function getCompras() {
    $.ajax({
        url: "cart-get-compras.php",
        method: "POST",
        success: function(results){
            $("#compras").html(results);
        }
    });
}

function setComprarLink() {
    $.ajax({
        url: "checkout.php",
        method: "POST",
        success: function(results){
            $('#comprar-link').attr("href", results);
        }
    });
}

function refresh(){
    getProducts();
    getSum();
    getCompras();
    setComprarLink();
}

function borrar(id){
    $.ajax({
        url: "products-delete.php",
        method: "POST",
        data: { id: id},
        success: function(results){
            refresh();
        }
    });
};

function agregar(id) {
    $.ajax({
        url: "product-info.php",
        method: "POST",
        data: { id: id },
        success: function(results){
            $('#shopModal #body').html(results);
            $('#shopModal #add').data('id', id);
            $('#shopModal #add').data('id', id);
            $('#shopModal #addGoToCart').data('id', id);
            $('#shopModal').modal('show');
        },
        error: function(xhr, status, error) {

        }
    });
}

function add(btn) {
    console.log($(btn).data('id'));
    console.log($(btn).data('cantidad'));

    $.ajax({
        url: "cart-add.php",
        method: "POST",
        data: {
            id: $(btn).data('id'),
            cantidad: $('#shopModal #cantidad').val()
        },
        success: function(results){
            getCompras();
        },
        error: function(xhr, status, error) {

        }
    });
}

function addGoToCart(btn) {
    console.log($(btn).data('id'));

}