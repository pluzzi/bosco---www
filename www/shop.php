<?php
    $id_page = 2;
    session_start();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('head.php'); ?>
</head>

<!-- Preloader -->
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>

<body>

    <div class="sb-site-container">
        <div class="boxed">
            <?php include('header.php'); ?>

            <?php include('menu.php'); ?>

            <header class="main-header">
                <div class="container">
                    <h1 class="page-title">Tienda</h1>
                </div>
            </header>

            <div class="container">
                <div class="row">
                    <div class="col-md-3 hidden-sm hidden-xs">
                        <div class="ec-filters-menu">
                            <h3 class="section-title no-margin-top">Filtros</h3>
                            <h4>Seleccione una Marca</h4>
                            <ul>
                                <li><a href="javascript:void(0);" class="filter" data-filter="all">Todas</a></li>
                                <?php
                                    include("config/database-config.php");

                                    $sql = "SELECT id, codigo, descripcion FROM marcas";
                                    $results = mysqli_query($conn, $sql);

                                    while($row = mysqli_fetch_array($results)){
                                        echo '<li><a class="filter" data-id="'.$row['id'].'" data-filter=".marca-'.$row['codigo'].'">'.$row['descripcion'].'</a></li>';
                                    }

                                    mysqli_free_result($results);
                                    mysqli_close($conn);

                                ?>
                            </ul>

                        </div>
                        <h3 class="section-title">Ultimas Ofertas</h3>
                        <div class="bxslider-controls">
                            <span id="bx-prev5"></span>
                            <span id="bx-next5"></span>
                        </div>
                        <ul class="bxslider" id="offers">
                            <?php
                            include("config/database-config.php");

                            $sql = "SELECT * FROM productos order by rand() limit 3";
                            $results = mysqli_query($conn, $sql);

                            while($row = mysqli_fetch_array($results)){
                                $sql = "SELECT * FROM imagenes_producto where producto='".$row['id']."' limit 1";
                                $imagenes = mysqli_query($conn, $sql);
                                $imagen = mysqli_fetch_array($imagenes);

                                if($imagen['img']!=null){
                                    $image = $imagen['img'];
                                    $im = new Imagick();
                                    $im->readimageblob($image);
                                    $im->thumbnailImage(300,150,true);
                                    $output = $im->getimageblob();
                                }


                                $sql = "SELECT codigo, dolar FROM marcas where id='".$row['brand_id']."' limit 1";
                                $marcas = mysqli_query($conn, $sql);
                                $marca = mysqli_fetch_array($marcas);

                                $sql = "select round(IFNULL(avg(puntos),0),1) as puntuacion from puntuaciones_productos where producto_id=".$row['id'];
                                $resultado = mysqli_query($conn, $sql);
                                $puntuacion = mysqli_fetch_array($resultado);

                                $visitas = rand(5, 20);

                                $stock = $row['mostrar_stock']==1 ? $row['stock'].' Unidades' : '';

                                $mas_vendido = $row['mas_vendido']==1 ? 'Más vendido' : '';

                                if(isset($_SESSION['id'])){
                                    $sql = "select descuento from descuentos_producto where producto=".$row['id']." and usuario=".$_SESSION['id'];
                                    $descuentos = mysqli_query($conn, $sql);

                                    if(mysqli_num_rows($descuentos)==0){
                                        $precio = $row['precio'] * $marca['dolar'];
                                        $precio_sin_descuento = "";
                                        $porcentaje_descuento = "";
                                    }else{
                                        $descuento = mysqli_fetch_array($descuentos);
                                        $precio = $row['precio']*(1-$descuento['descuento']/100) * $marca['dolar'];
                                        $precio_sin_descuento = '<span class="label-line-through">$ '.$row['precio'] * $marca['dolar'].'</span>';
                                        $porcentaje_descuento = '<span class="label label-danger pull-right">-'.$descuento['descuento'].'%</span>';
                                    }
                                }else{
                                    $precio = $row['precio'] * $marca['dolar'];
                                    $precio_sin_descuento = "";
                                    $porcentaje_descuento = "";
                                }

                                $envio_gratis = $row['envio_gratis']==1 ? 'Envío gratis' : '';

                                echo '<li>
                                            <div class="ec-box">
                                                <div class="ec-box-header">
                                                    <a href="product-detail.php?id='.$row['id'].'">'.$row['titulo'].'</a>
                                                    <span class="pull-right" style="margin-left: 10px">'.$visitas.' <i class="glyphicon glyphicon-eye-open" style="color: #c5b319;"></i></span>
                                                </div>
                                                <a href="product-detail.php?id='.$row['id'].'">
                                                    <img src="data:image/jpeg;base64,'.base64_encode( $output ).'" />
                                                </a>
                                                <div class="ec-box-footer" >
                                                    <span class="pull-right" style="margin-left: 10px">'.$puntuacion['puntuacion'].' <i class="glyphicon glyphicon-star" style="color: #c5b319;"></i></span>
                                                    <span class="" style="margin-right: 10px">'.$stock.'</span>
                                                    <span style="color: #d9534f;">'.$mas_vendido.'</span>
                                                </div>
                                                <div class="ec-box-footer">
                                                    <span class="label label-primary">$ '.$precio.'</span>
                                                    '.$precio_sin_descuento.'
                                                    '.$porcentaje_descuento.'
                                                </div>
                                                <div class="ec-box-footer">
                                                    <span style="color: #02c66c;">'.$envio_gratis.'</span>
                                                    <button onclick="comprar('.$row['id'].')" class="btn btn-ar btn-success btn-sm pull-right"><i class="fa fa-shopping-cart"></i> Agregar</button>
                                                </div>
                                            </div>
                                        </li>';
                            }

                            ?>
                        </ul>
                    </div>
                    <div class="col-md-9">
                        <div class="row" id="Container">
                            <?php
                                include("config/database-config.php");

                                $sql = "SELECT * FROM productos";
                                $results = mysqli_query($conn, $sql);

                                while($row = mysqli_fetch_array($results)){
                                    $sql = "SELECT * FROM imagenes_producto where producto='".$row['id']."' limit 1";
                                    $imagenes = mysqli_query($conn, $sql);
                                    $imagen = mysqli_fetch_array($imagenes);

                                    if($imagen['img']!=null){
                                        $image = $imagen['img'];
                                        $im = new Imagick();
                                        $im->readimageblob($image);
                                        $im->thumbnailImage(300,150,true);
                                        $output = $im->getimageblob();
                                    }


                                    $sql = "SELECT codigo, dolar FROM marcas where id='".$row['brand_id']."' limit 1";
                                    $marcas = mysqli_query($conn, $sql);
                                    $marca = mysqli_fetch_array($marcas);

                                    $sql = "select round(IFNULL(avg(puntos),0),1) as puntuacion from puntuaciones_productos where producto_id=".$row['id'];
                                    $resultado = mysqli_query($conn, $sql);
                                    $puntuacion = mysqli_fetch_array($resultado);

                                    $visitas = rand(5, 20);

                                    $stock = $row['mostrar_stock']==1 ? $row['stock'].' Unidades' : '';

                                    $mas_vendido = $row['mas_vendido']==1 ? 'Más vendido' : '';

                                    if(isset($_SESSION['id'])){
                                        $sql = "select descuento from descuentos_producto where producto=".$row['id']." and usuario=".$_SESSION['id'];
                                        $descuentos = mysqli_query($conn, $sql);

                                        if(mysqli_num_rows($descuentos)==0){
                                            $precio = $row['precio'] * $marca['dolar'];
                                            $precio_sin_descuento = "";
                                            $porcentaje_descuento = "";
                                        }else{
                                            $descuento = mysqli_fetch_array($descuentos);
                                            $precio = $row['precio']*(1-$descuento['descuento']/100) * $marca['dolar'];
                                            $precio_sin_descuento = '<span class="label-line-through">$ '.$row['precio'] * $marca['dolar'].'</span>';
                                            $porcentaje_descuento = '<span class="label label-danger pull-right">-'.$descuento['descuento'].'%</span>';
                                        }
                                    }else{
                                        $precio = $row['precio'] * $marca['dolar'];
                                        $precio_sin_descuento = "";
                                        $porcentaje_descuento = "";
                                    }

                                    $envio_gratis = $row['envio_gratis']==1 ? 'Envío gratis' : '';

                                    echo '<div class="col-sm-4 mix marca-'.$marca['codigo'].'">
                                            <div class="ec-box">
                                                <div class="ec-box-header">
                                                    <a href="product-detail.php?id='.$row['id'].'">'.$row['titulo'].'</a>
                                                    <span class="pull-right" style="margin-left: 10px">'.$visitas.' <i class="glyphicon glyphicon-eye-open" style="color: #c5b319;"></i></span>
                                                </div>
                                                <a href="product-detail.php?id='.$row['id'].'">
                                                    <img src="data:image/jpeg;base64,'.base64_encode( $output ).'" />
                                                </a>
                                                <div class="ec-box-footer" >
                                                    <span class="pull-right" style="margin-left: 10px">'.$puntuacion['puntuacion'].' <i class="glyphicon glyphicon-star" style="color: #c5b319;"></i></span>
                                                    <span class="" style="margin-right: 10px">'.$stock.'</span>
                                                    <span style="color: #d9534f;">'.$mas_vendido.'</span>
                                                </div>
                                                <div class="ec-box-footer">
                                                    <span class="label label-primary">$ '.$precio.'</span>
                                                    '.$precio_sin_descuento.'
                                                    '.$porcentaje_descuento.'
                                                </div>
                                                <div class="ec-box-footer">
                                                    <span style="color: #02c66c;">'.$envio_gratis.'</span>
                                                    <button onclick="agregar('.$row['id'].')" class="btn btn-ar btn-success btn-sm pull-right"><i class="fa fa-shopping-cart"></i> Agregar</button>
                                                </div>
                                            </div>
                                        </div>';
                                }

                            ?>

                        </div>
                    </div>
                </div>
            </div> <!-- container -->

            <?php include('footer.php'); ?>

        </div> <!-- boxed -->
    </div> <!-- sb-site -->

    <div id="back-top">
        <a href="#header"><i class="fa fa-chevron-up"></i></a>
    </div>

    <?php include('scripts.php'); ?>

    <script>
        $(document).ready(function () {
            var id = "<?php
                if(isset($_GET['id'])){
                    echo $_GET['id'];
                }else{
                    echo '';
                }
            ?>";

            if(id!=''){
                var filter = ".filter[data-id='"+id+"']";
                $( filter ).trigger( "click" );
            }

        });
    </script>

    <script src="assets/js/shop.js"></script>

</body>

</html>
