<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h2 class="right-line">Productos Recientes</h2>
        </div>
        <?php 
            include("config/database-config.php");
            
            $sql = "SELECT
            t.id,
            t.titulo,
            (SELECT img FROM trabajos_imagenes where id_trabajo = t.id order by id asc limit 1) as img
            FROM trabajos t order by t.id desc limit 6";
            
            $results = mysqli_query($conn, $sql);
            
            while($row = mysqli_fetch_array($results)){
                if($row['img']!=null){
                    $image = $row['img'];
                    $im = new Imagick();
                    $im->readimageblob($image);
                    $im->thumbnailImage(300,300,true);
                    $output = $im->getimageblob();
                }
                echo '<div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="img-caption-ar wow fadeInUp">
                            <img alt="Image" style="height: 300px;" class="img-responsive" src="data:image/jpeg;base64,'.base64_encode( $output ).'" />
                            <div class="caption-ar">
                                <div class="caption-content">
                                    <a href="recent-product-detail.php?id='.$row['id'].'" class="animated fadeInDown"><i class="fa fa-search"></i>Más informacón</a>
                                    <h4 class="caption-title">'.$row['titulo'].'</h4>
                                </div>
                            </div>
                        </div>
                    </div>';
            }

            mysqli_free_result($results);
            mysqli_close($conn);

        ?>

    </div>
</div>