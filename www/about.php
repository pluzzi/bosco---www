<section class="margin-bottom">
    <div class="row">
        <div class="col-md-12">
            <h2 class="right-line">A qué nos dedicamos?</h2>
        </div>
        <?php 
            include("config/database-config.php");
            
            $sql = "SELECT * FROM nosotros limit 6";
            $results = mysqli_query($conn, $sql);
            
            while($row = mysqli_fetch_array($results)){
                echo '<div class="col-md-4 col-sm-6">
                        <div class="text-icon wow fadeInUp">
                            <span class="icon-ar icon-ar-lg"><i class="'.$row['icono'].'"></i></span>
                            <div class="text-icon-content">
                                <h3 class="no-margin">'.$row['titulo'].'</h3>
                                <p>'.$row['descripcion'].'</p>
                            </div>
                        </div>
                    </div>';
            }
                
            mysqli_free_result($results);
            mysqli_close($conn);

        ?>
        
    </div> <!-- row -->
</section>
