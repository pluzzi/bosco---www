<?php
    $id_page = -1;
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('head.php'); ?>
</head>

<!-- Preloader -->
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>

<body>

    <div class="sb-site-container">
        <div class="boxed">
            <?php include('header.php'); ?>

            <?php include('menu.php'); ?>

            <header class="main-header">
                <div class="container">
                    <h1 class="page-title">Detalle del Producto Reciente</h1>

                    <ol class="breadcrumb pull-right">
                        <li>Productos Recientes</li>
                        <li class="active">Detalles</li>
                    </ol>
                </div>
            </header>

            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <div class="row" style="margin-bottom: 50px;">
                            <div class="col-md-12">
                                <div id="carousel-example-captions" class="carousel carousel-images slide" data-ride="carousel">
                                    <ol class="carousel-indicators">
                                        <?php 
                                            include("config/database-config.php");
                                            
                                            $sql = "SELECT * FROM trabajos_imagenes where id_trabajo = ".$_GET['id'];
                                            $results = mysqli_query($conn, $sql) or die (mysqli_error());
                                            $i = 0;

                                            while($row = mysqli_fetch_array($results)){
                                                $active = $i==0 ? 'class = "active"' : '';

                                                echo '<li data-target="#carousel-example-captions" data-slide-to="'.$i++.'" '.$active.'></li>';
                                                
                                                $primero = false;
                                            }

                                            mysqli_free_result($results);
                                            mysqli_close($conn);
                                        ?>
                                    </ol>
                                    <div class="carousel-inner">
                                        <?php 
                                            include("config/database-config.php");
                                            
                                            $sql = "SELECT * FROM trabajos_imagenes where id_trabajo = ".$_GET['id'];
                                            $results = mysqli_query($conn, $sql) or die (mysqli_error());
                                            $primero = true;

                                            while($row = mysqli_fetch_array($results)){
                                                $active = $primero ? "active" : "";

                                                echo '<div class="item '.$active.'">
                                                        <img src="data:image/jpeg;base64,'.base64_encode( $row['img'] ).'" />
                                                        <!--<div class="carousel-caption animated fadeInUpBig">
                                                            <h3></h3>
                                                            <p></p>
                                                        </div>-->
                                                    </div>';
                                                $primero = false;
                                            }

                                            mysqli_free_result($results);
                                            mysqli_close($conn);
                                        ?>
                                        
                                    </div>
                                    <a class="left carousel-control" href="#carousel-example-captions" data-slide="prev">
                                        <span class="glyphicon glyphicon-chevron-left"></span>
                                    </a>
                                    <a class="right carousel-control" href="#carousel-example-captions" data-slide="next">
                                        <span class="glyphicon glyphicon-chevron-right"></span>
                                    </a>
                                </div>
                            </div>
                            <hr class="dotted hidden-md hidden-lg">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-default portfolio-item-panel">
                            <?php 
                                include("config/database-config.php");
                                
                                $sql = "SELECT * FROM trabajos where id = ".$_GET['id'];
                                $results = mysqli_query($conn, $sql) or die (mysqli_error());

                                $row = mysqli_fetch_array($results);

                            ?>
                            <div class="panel-heading">Detalles del Producto</div>
                            <div class="panel-body" >
                                <h4 class="no-margin-bottom">Datos</h4>
                                <ul class="list-unstyled">
                                    <li><strong>Título:</strong> <?php echo $row['titulo']; ?></li>
                                </ul>
                                <h4 class="no-margin-bottom">Descripción</h4>
                                <p><?php echo $row['descripcion']; ?></p>
                            </div>

                            <?php
                                mysqli_free_result($results);
                                mysqli_close($conn);
                            ?>
                        </div>
                    </div>
                </div>
            </div>


            <?php include('footer.php'); ?>

        </div> <!-- boxed -->
    </div> <!-- sb-site -->


    <div id="back-top">
        <a href="#header"><i class="fa fa-chevron-up"></i></a>
    </div>

    <?php include('scripts.php'); ?>

</body>

</html>
