<section class="margin-top">
    <h2 class="section-title">Nuestros Clientes</h2>
    <div class="row">
        <div class="col-md-6">
            <div class="bxslider-controls">
                <span id="bx-prev5"></span>
                <span id="bx-next5"></span>
            </div>

            <ul class="bxslider" id="home-block">
                <?php
                    include("config/database-config.php");

                    $sql = "SELECT * FROM comentarios_clientes limit 4";
                    $results = mysqli_query($conn, $sql);

                    while($row = mysqli_fetch_array($results)){
                        echo '<li>
                                <blockquote class="blockquote-color">
                                    <p>'.$row['comentario'].'</p>
                                    <footer>'.$row['nombre'].', '.$row['roll'].'</footer>
                                </blockquote>
                            </li>';
                    }

                ?>
            </ul>
        </div>

        <div class="col-md-6">
            <?php
                include("config/database-config.php");

                $sql = "SELECT * FROM imagenes_clientes";
                $results = mysqli_query($conn, $sql);
                $i = 1;

                while($row = mysqli_fetch_array($results)){
                    if($i==1){
                        echo '<div class="row">';
                    }

                    $image = $row['imagen'];
                    $im = new Imagick();
                    $im->readimageblob($image);
                    $im->thumbnailImage(150,150,true);
                    $imagen = $im->getimageblob();

                    echo '<div class="col-md-3 col-sm-3 col-xs-6">
                            <img class="img-responsive" src="data:image/jpeg;base64,'.base64_encode( $imagen ).'" />
                        </div>';

                    if($i==4){
                        echo '</div>';
                        $i = 1;
                    }else{
                        $i++;
                    }
                }

            ?>

        </div>
    </div>
</section>