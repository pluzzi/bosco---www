<div class="container">
    <h2 class="right-line">Available Plans</h2>
    <section class="css-section">
        <div class="row">
            <div class="col-md-4 col-sm-6">
                <div class="pricign-box wow fadeInUp animation-delay-7">
                    <div class="pricing-box-header">
                        <h2>Personal</h2>
                        <p>nisi anim mollit in labore ut esse</p>
                    </div>
                    <div class="pricing-box-price">
                        <h3>$ 10 <sub>/ month</sub> </h3>
                    </div>
                    <div class="pricing-box-content">
                        <ul>
                            <li><i class="fa fa-inbox"></i> Exercitation in id in officia.</li>
                            <li><i class="fa fa-cloud-download"></i> Exercitation in id in officia.</li>
                            <li><i class="fa fa-dashboard"></i> Exercitation in id in officia.</li>
                            <li><i class="fa fa-sitemap"></i> Exercitation in id in officia.</li>
                            <li><i class="fa fa-shopping-cart"></i> Exercitation in id in officia.</li>
                        </ul>
                    </div>
                    <div class="pricing-box-footer">
                        <a href="#" class="btn btn-ar btn-default">Order Now</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="pricign-box pricign-box-pro wow fadeInUp animation-delay-9">
                    <div class="pricing-box-header">
                        <h2>Professional</h2>
                        <p>nisi anim mollit in labore ut esse</p>
                    </div>
                    <div class="pricing-box-price">
                        <h3>$ 25 <sub>/ month</sub> </h3>
                    </div>
                    <div class="pricing-box-content">
                        <ul>
                            <li><i class="fa fa-inbox"></i> Exercitation in id in officia.</li>
                            <li><i class="fa fa-cloud-download"></i> Exercitation in id in officia.</li>
                            <li><i class="fa fa-dashboard"></i> Exercitation in id in officia.</li>
                            <li><i class="fa fa-sitemap"></i> Exercitation in id in officia.</li>
                            <li><i class="fa fa-shopping-cart"></i> Exercitation in id in officia.</li>
                        </ul>
                    </div>
                    <div class="pricing-box-footer">
                        <a href="#" class="btn btn-ar btn-primary">Order Now</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="pricign-box wow fadeInUp animation-delay-8">
                    <div class="pricing-box-header">
                        <h2>Ultimate</h2>
                        <p>nisi anim mollit in labore ut esse</p>
                    </div>
                    <div class="pricing-box-price">
                        <h3>$ 10 <sub>/ month</sub> </h3>
                    </div>
                    <div class="pricing-box-content">
                        <ul>
                            <li><i class="fa fa-inbox"></i> Exercitation in id in officia.</li>
                            <li><i class="fa fa-cloud-download"></i> Exercitation in id in officia.</li>
                            <li><i class="fa fa-dashboard"></i> Exercitation in id in officia.</li>
                            <li><i class="fa fa-sitemap"></i> Exercitation in id in officia.</li>
                            <li><i class="fa fa-shopping-cart"></i> Exercitation in id in officia.</li>
                        </ul>
                    </div>
                    <div class="pricing-box-footer">
                        <a href="#" class="btn btn-ar btn-default">Order Now</a>
                    </div>
                </div>
            </div>
        </div> <!-- row -->
    </section>
</div>