<nav class="navbar navbar-default navbar-header-full navbar-dark yamm navbar-static-top fixed-top" role="navigation" id="header">
    <div class="container ">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <i class="fa fa-bars"></i>
            </button>
            <a id="ar-brand" class="navbar-brand hidden-lg hidden-md hidden-sm navbar-dark" href="index.php" style="text-transform: capitalize;">
                Bosco
            </a>
        </div> <!-- navbar-header -->

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="<?php if($id_page==1){ echo "active"; } ?>"><a href="index.php">Inicio</a></li>
                <li class="<?php if($id_page==2){ echo "active"; } ?>"><a href="shop.php">Tienda</a></li>
                <li class="<?php if($id_page==3){ echo "active"; } ?>"><a href="about-us.php">Nosotros</a></li>
                <li class="<?php if($id_page==4){ echo "active"; } ?>"><a href="workshop.php">Taller</a></li>
                <li class="<?php if($id_page==5){ echo "active"; } ?>"><a href="contact.php">Contacto</a></li>
            </ul>
        </div><!-- navbar-collapse -->
    </div><!-- container -->
</nav>