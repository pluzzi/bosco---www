<?php
    session_start();
    include('config/database-config.php');

    $sql = "select
            d.id,
            (select i.img from imagenes_producto i where i.producto = d.producto limit 1) as img,
            (select p.titulo from productos p where p.id = d.producto) as producto,
            d.cantidad,
            ObtenerPrecioProducto(producto,usuario)*d.cantidad as precio
            from carrito_detalle d
            left join carrito_cabecera c on c.id = d.cabecera
            where c.usuario = ".$_SESSION['id']."
            and c.collection_status is null
            and c.id = ".$_SESSION['orden'];

    $result = mysqli_query($conn, $sql);

    while ($row = mysqli_fetch_assoc($result)) {

        if($row['img']!=null){
            $image = $row['img'];
            $im = new Imagick();
            $im->readimageblob($image);
            $im->thumbnailImage(100,60,true);
            $output = $im->getimageblob();
        }else{
            $output = null;
        }

        echo '<tr>
                    <td>'.$row['id'] .'</td>
                    <td><img alt="image" class="img-profile-size" src="data:image/jpeg;base64,'.base64_encode( $output ).'" />
                    <td>'.$row['producto'] .'</td>
                    <td>'.$row['cantidad'] .'</td>
                    <td>'.$row['precio'] .'</td>
                    <td>
                        <button onclick="borrar('.$row['id'].')" class="btn btn-warning btn-sm">
                            <i  class="fa fa-minus-circle"></i>
                        </button>
                    </td>
                </tr>';
    }
?>