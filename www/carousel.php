<div class="row carousel slide">
    <div class="col-lg-offset-2 col-lg-8">
        <div id="myCarousel" class="carousel slide" data-ride="carousel" >
            <!-- Indicators -->

            <ol class="carousel-indicators">
                <?php
                    include("config/database-config.php");

                    $sql = "SELECT 1 FROM carrusel";
                    $results = mysqli_query($conn, $sql);
                    $i = 0;

                    while($row = mysqli_fetch_array($results)){
                        $active = $i==0 ? "active" : "";
                        echo '<li data-target="#myCarousel" data-slide-to="'.$i.'" ></li>';
                        $i++;
                    }

                ?>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" style="width: 100%; height: 100%;">
                <?php
                include("config/database-config.php");

                $sql = "SELECT * FROM carrusel";
                $results = mysqli_query($conn, $sql);
                $i = 0;

                while($row = mysqli_fetch_array($results)){
                    $active = $i==0 ? "active" : "";
                    echo '<div class="item '.$active.'">
                                    <a href="shop.php?id='.$row['brand_id'].'"><img alt="'.$row['texto'].'" src="data:image/jpeg;base64,'.base64_encode( $row['img'] ).'" /></a>
                                </div>';
                    $i++;
                }

                ?>
            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>