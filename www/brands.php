<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h2 class="right-line">Marcas</h2>
        </div>
        <?php 
            include("config/database-config.php");
            
            $sql = "SELECT
            id,
            codigo,
            descripcion,
            logo
            FROM marcas";
            
            $results = mysqli_query($conn, $sql);
            
            while($row = mysqli_fetch_array($results)){
                if($row['logo']!=null){
                    $image = $row['logo'];
                    $im = new Imagick();
                    $im->readimageblob($image);
                    $im->thumbnailImage(200,200,true);
                    $output = $im->getimageblob();
                }
                echo '<div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="img-caption-ar wow fadeInUp" style="height: 120px; !important;" >
                            <img alt="Image" style="height: 110px; !important;" class="img-responsive" src="data:image/jpeg;base64,'.base64_encode( $output ).'"  />
                            <div class="caption-ar">
                                <div class="caption-content">
                                    <a href="shop.php?id='.$row['id'].'" class="animated fadeInDown"><i class="fa fa-search"></i>Ver Productos</a>
                                    <h4 class="caption-title">'.$row['descripcion'].'</h4>
                                </div>
                            </div>
                        </div>
                    </div>';
            }

            mysqli_free_result($results);
            mysqli_close($conn);

        ?>

    </div>
</div>