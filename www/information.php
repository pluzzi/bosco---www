<div class="container margin-bottom">
    <div class="row">
        <div class="col-md-12">
            <h2 class="right-line no-margin-bottom">Información</h2>
        </div>
        <div class="col-md-12 animated fadeInLeft animation-delay-8">
            <h3>Acerca de nosotros</h3>
            <?php
                include('config/database-config.php');
                $sql = "select * from informacion limit 1";
                $results = $conn->query($sql);
                $informacion = mysqli_fetch_assoc($results);

                echo $informacion['descripcion'];

            ?>
        </div>

    </div>
</div> <!-- container -->