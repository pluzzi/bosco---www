<?php
    $id_page = 4;
    session_start();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('head.php'); ?>
</head>

<!-- Preloader
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>-->

<body>

    <div class="sb-site-container">
        <div class="boxed">
            <?php include('header.php'); ?>

            <?php include('menu.php'); ?>

            <header class="main-header">
                <div class="container">
                    <h1 class="page-title">Talleres</h1>
                </div>
            </header>

            <div class="container">
                <div class="row">
                    <div class="col-md-5">
                        <h3>Reparación de Motores</h3>
                        <p>Brindamos el servicio de reparación y mantenimiento de todo tipo de motores eléctricos y máquinas electromecánicas.</p>
                        <ul>
                            <li>Motores de Corriente Alterna y Corriente Continua.</li>
                            <li>Motores antiexplosivos.</>
                            <li>Generadores.</li>
                            <li>Motores de 2 y 3 velocidades.</li>
                            <li>Frenos electromagnéticos.</li>
                        </ul>
                        <p>Todos los trabajos eléctricos y mecánicos son realizados en nuestro taller, garantizando mínimos plazos de entrega en las reparaciones.</p>
                        <ul>
                            <li>Bobinaje, recálculo y cambio de características.</li>
                            <li>Reparaciones Mecánicas.</li>
                            <li>Maquinado.</li>
                            <li>Balanceo dinámico.</li>
                            <li>Reconstrucción de ejes.</li>
                        </ul>

                        <p>Somos Asistentes Técnicos Autorizados (ATA) de la firma WEG desde 2011, lo que nos autoriza a realizar todo tipo de reparaciones y modificaciones en su completa línea de productos.</p>

                        <p><a href="contact.php">Solicitar más información de motores.</a></p>

                        <h3>Reparación de Electrobombas</h3>
                        <p>
                            Contamos con un taller especializado en la reparación de electrobombas centrífugas y sumergibles.
                            Al ser distribuidores de diferentes marcas de electrobombas contamos con todas las líneas de repuestos originales, asegurando que la calidad original de los equipos confiados a nuestra firma sea renovada eficientemente
                        </p>
                        <p><a href="contact.php">Solicitar más información de electrobombas.</a></p>

                        <h3>Reparación de Drives</h3>
                        <p>
                            Al ser asistentes Técnicos de la firma WEG, realizamos reparaciones de toda la línea de Variadores y Arrancadores electrónicos.
                        </p>
                        <p><a href="contact.php">Solicitar más información de drivers.</a></p>
                    </div>

                    <div class="col-md-3">
                        <img src="assets/img/taller/taller01.jpg" class="taller-img">
                        <img src="assets/img/taller/taller02.jpg" class="taller-img">
                        <img src="assets/img/taller/taller03.jpg" class="taller-img" style="height: 250px !important;">
                        <img src="assets/img/taller/taller04.jpg" class="taller-img">
                    </div>

                </div> <!-- row -->
            </div> <!-- container -->

            <?php include('footer.php'); ?>

        </div> <!-- boxed -->
    </div> <!-- sb-site -->

    <div id="back-top">
        <a href="#header"><i class="fa fa-chevron-up"></i></a>
    </div>

    <?php include('scripts.php'); ?>

</body>

</html>
