<?php
    $id_page = 5;
    session_start();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('head.php'); ?>
</head>

<!-- Preloader
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>-->

<body>

    <div class="sb-site-container">
        <div class="boxed">
            <?php include('header.php'); ?>

            <?php include('menu.php'); ?>

            <header class="main-header">
                <div class="container">
                    <h1 class="page-title">Contacto</h1>
                </div>
            </header>

            <div class="container">
                <div class="row">
                    <div class="alert alert-success" id="statusok" style="display: none !important;">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <strong><i class="fa fa-check"></i>
                            Su mensaje ha sido enviado. Nos pondremos en contacto.
                    </div>
                    <div class="alert alert-warning" id="statuserror" style="display: none !important;">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <strong><i class="fa fa-warning"></i>
                            Ha ocurrido un error. Intentelo nuevamente.
                    </div>
                    <div class="col-md-12">
                        <h2 class="section-title no-margin-top">Envíanos un Mensaje</h2>
                    </div>
                    <div class="col-md-8">
                        <section>
                            <div class="form-group">
                                <label>Nombre</label>
                                <input type="text" class="form-control" id="nombre">
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" class="form-control" id="email">
                            </div>
                            <div class="form-group">
                                <label>Mesagge</label>
                                <textarea class="form-control" id="mensaje" rows="8"></textarea>
                            </div>
                            <button class="btn btn-ar btn-primary" id="send">Enviar</button>
                            <div class="clearfix"></div>

                        </section>
                    </div>

                    <div class="col-md-4">
                        <section>
                            <div class="panel panel-primary">
                                <div class="panel-heading"><i class="fa fa-envelope-o"></i> Información</div>
                                <div class="panel-body">
                                    <h4 class="section-title no-margin-top">Contacto</h4>
                                    <address>
                                        <strong>Bosco Hermanos.</strong><br>
                                        Bartolomé Mitre 1126<br>
                                        S2252 Gálvez, Santa Fe<br>
                                        <abbr title="Phone">Tel.:</abbr> +54 9 3404 639316 <br>
                                        E-Mail: <a href="">info@boscohnos.com.ar</a>
                                    </address>

                                    <!-- Business Hours -->
                                    <h4 class="section-title no-margin-top">Horarios de atención</h4>
                                    <ul class="list-unstyled">
                                        <li><strong>Lunes-Viernes:</strong> 7:00 – 20:00</li>
                                        <li><strong>Sabados:</strong> 7:00 – 12:00</li>
                                        <li><strong>Domingos:</strong> Cerrado</li>
                                    </ul>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>

                <hr class="dotted">

                <section>
                    <div class="well well-sm">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3382.6983393357023!2d-61.22609108528587!3d-32.02328413307161!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x95b58c706901e547%3A0xc9913fc976fc901c!2sBartolom%C3%A9%20Mitre%201128%2C%20S2252%20G%C3%A1lvez%2C%20Santa%20Fe!5e0!3m2!1ses-419!2sar!4v1580941602359!5m2!1ses-419!2sar" width="100%" height="350" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                    </div>
                </section>
            </div> <!-- container -->

            <?php include('footer.php'); ?>

        </div> <!-- boxed -->
    </div> <!-- sb-site -->


    <div id="back-top">
        <a href="#header"><i class="fa fa-chevron-up"></i></a>
    </div>

    <?php include('scripts.php'); ?>

    <script>
        $('#send').click(function () {
            var nombre = $('#nombre').val();
            var email = $('#email').val();
            var mensaje = $('#mensaje').val();

            $.ajax({
                url: "send-message.php",
                method: "POST",
                data: {nombre: nombre, email: email, mensaje: mensaje},
                success: function(result){
                    $('#statusok').css("display", "block");
                },
                error: function (xhr, status, error) {
                    $('#statuserror').css("display", "block");
                }
            });
        });
    </script>

</body>

</html>
