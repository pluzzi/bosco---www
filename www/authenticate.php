<?php
    $email = $_POST['email'];
    $password = $_POST['password'];

    include 'config/database-config.php';

    $sql = "SELECT * FROM usuarios_ec WHERE email = '$email'";
    $result = mysqli_query($conn, $sql);
    $usuario = mysqli_fetch_array($result);

    if (mysqli_num_rows($result)==0){
        header('HTTP/1.1 500 Internal Server Booboo');
        header('Content-Type: application/json; charset=UTF-8');
        die(json_encode(array('message' => 'El E-Mail no es válidos.', 'code' => 1 )));
    } else {
        if ($password == $usuario['password']) {
            session_start();
            $_SESSION['id'] = $usuario['id'];
            $_SESSION['nombre'] = $usuario['nombre'];
            $_SESSION['apellido'] = $usuario['apellido'];
            $_SESSION['email'] = $usuario['email'];

        } else {
            header('HTTP/1.1 500 Internal Server Booboo');
            header('Content-Type: application/json; charset=UTF-8');
            die(json_encode(array('message' => 'El Password ingresado no es válido.', 'code' => 1 )));
        }
    }

    mysqli_free_result($result);
    mysqli_close($conn);

?>