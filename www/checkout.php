<?php
    if(isset($_GET['external_reference'])){
        include('config/database-config.php');

        $collection_id = $_GET['collection_id']!='null' ? $_GET['collection_id'] : '';
        $collection_status = $_GET['collection_status']!='null' ? $_GET['collection_status'] : '';
        $external_reference = $_GET['external_reference']!='null' ? $_GET['external_reference'] : '';
        $payment_type = $_GET['payment_type']!='null' ? $_GET['payment_type'] : '';
        $merchant_order_id = $_GET['merchant_order_id']!='null' ? $_GET['merchant_order_id'] : '';
        $preference_id = $_GET['preference_id']!='null' ? $_GET['preference_id'] : '';
        $site_id = $_GET['site_id']!='null' ? $_GET['site_id'] : '';
        $processing_mode = $_GET['processing_mode']!='null' ? $_GET['processing_mode'] : '';
        $merchant_account_id = $_GET['merchant_account_id']!='null' ? $_GET['merchant_account_id'] : '';

        $sql = "insert into pagos(collection_id,collection_status,external_reference,payment_type,merchant_order_id,preference_id,site_id,processing_mode,merchant_account_id)
                values('$collection_id','$collection_status','$external_reference','$payment_type','$merchant_order_id','$preference_id','$site_id','$processing_mode','$merchant_account_id')";

        $result = mysqli_query($conn, $sql);

        $sql = "update carrito_cabecera set
                collection_status = '$collection_status'
                where id=$external_reference";

        $result = mysqli_query($conn, $sql);

        if($collection_status=='approved'){
            $sql = "call guardar_historial(".$_SESSION['orden'].")";
            $result = mysqli_query($conn, $sql);

            $sql = "call crear_notificacion(".$_SESSION['id'].", 'Se realizó una compra.', ".$_SESSION['orden'].")";
            $result = mysqli_query($conn, $sql);

            header("Location: pay-done.php?status=approved");

        }else{
            header("Location: pay-done.php?status=error");
        }

    }else{
        session_start();
        include('config/mercadopago-config.php');
        include('config/database-config.php');

        $sql = "select
            d.id,
            (select p.titulo from productos p where p.id = d.producto) as producto,
            d.cantidad,
            ObtenerPrecioProducto(producto,usuario) as precio
            from carrito_detalle d
            left join carrito_cabecera c on c.id = d.cabecera
            where c.usuario = ".$_SESSION['id']."
            and c.collection_status is null
            and c.id = ".$_SESSION['orden'];

        $result = mysqli_query($conn, $sql);

        $items = array();

        while ($row = mysqli_fetch_assoc($result)) {
            # Create an item object
            $item = new MercadoPago\Item();
            $item->id = $row['id'];
            $item->title = $row['producto'];
            $item->quantity = $row['cantidad'];
            $item->currency_id = "ARS";
            $item->unit_price = $row['precio'];

            array_push($items, $item);
        }

        # Create a preference object
        $preference = new MercadoPago\Preference();

        # Create a payer object
        $payer = new MercadoPago\Payer();
        $payer->name = $_SESSION['nombre'];
        $payer->surname = $_SESSION['apellido'];
        $payer->email = $_SESSION['email'];

        # Setting preference properties
        $preference->items = $items;
        $preference->payer = $payer;

        $protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === 0 ? 'https://' : 'http://';
        $url = $protocol.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

        $preference->back_urls = array(
            "success" => $url,
            "failure" => $url,
            "pending" => $url
        );

        $preference->auto_return = "approved";
        $preference->binary_mode = true;
        $preference->external_reference = $_SESSION['orden'];

        # Save and posting preference
        $preference->save();

        echo $preference->init_point;
    }
?>