<?php
    $id_page = 2;
    session_start();
    if(!isset($_SESSION['id'])){
        header("Location: shop.php");
    }
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <?php include('head.php'); ?>
</head>

<!-- Preloader -->
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>

<body>

<div class="sb-site-container">
    <div class="boxed">
        <?php include('header.php'); ?>

        <?php include('menu.php'); ?>

        <header class="main-header">
            <div class="container">
                <h1 class="page-title">Compras</h1>
            </div>
        </header>

        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#ID</th>
                            <th>Imágen</th>
                            <th>Producto</th>
                            <th>Cantidad</th>
                            <th>Precio</th>
                        </tr>
                        </thead>
                        <tbody id="results">

                        </tbody>
                    </table>
                </div>
                <div class="col-md-4">
                    <div class="e-price" id="sum"></div>
                    <a id="comprar-link" class="btn btn-ar btn-block btn-success"><i class="fa fa-shopping-cart"></i> Comprar</a>
                </div>
            </div>
        </div>

        <?php include('footer.php'); ?>

    </div> <!-- boxed -->
</div> <!-- sb-site -->


<div id="back-top">
    <a href="#header"><i class="fa fa-chevron-up"></i></a>
</div>

<?php include('scripts.php'); ?>

</body>

<script>
    $(document).ready(function() {
        refresh();
    });
</script>

</html>
