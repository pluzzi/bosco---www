create table configuracion(
	id int not null auto_increment,
    nombre varchar(1024) not null,
    nemonico varchar(256) not null,
    valor text not null,
    valor2 text null,
    primary key(id)
);

insert into configuracion(nombre,nemonico,valor)
values('Lema de la empresa','LEMA','Bobinado de motores');

insert into configuracion(nombre,nemonico,valor,valor2)
values('Facebook','SOCIAL','http://www.facebook.com','facebook');

insert into configuracion(nombre,nemonico,valor,valor2)
values('Twitter','SOCIAL','http://www.twitter.com','twitter');