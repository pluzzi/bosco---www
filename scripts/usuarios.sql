create table usuarios(
	id int not null auto_increment,
    nombre varchar(1024) not null,
    password text not null,
    img longblob null,
    hash_password text null,
    primary key(id)
);

insert into usuarios(nombre, password)
values('patricio@gmail.com', '123');