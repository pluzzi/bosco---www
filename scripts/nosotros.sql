create table nosotros(
	id int not null auto_increment,
    titulo varchar(1024) not null,
    descripcion varchar(2048) not null,
    icono varchar(256) not null,
    primary key(id)
);