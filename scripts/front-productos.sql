create table front_productos(
	id int not null auto_increment,
    descripcion varchar(2048) not null,
    icono varchar(256) not null,
    img longblob null,
    primary key(id)
);

create table front_producto_detalles(
	id int not null auto_increment,
    titulo varchar(1024) not null,
    descripcion varchar(2048) not null,
    producto_id int not null,
    primary key(id)
);